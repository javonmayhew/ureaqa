import { Injectable } from '@angular/core';
import {Platform, AlertController } from '@ionic/angular';
import { LocalNotifications, ILocalNotification } from '@ionic-native/local-notifications/ngx';
import * as moment from 'moment';
import { MessageBundle } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class NotifyService {

  notifyTime: any;
  notifications: any[] = [];
  days: any[];
  chosenHours: number;
  chosenMinutes: number;

  constructor(public platform: Platform, public alertCtrl: AlertController, public localNotifications: LocalNotifications) {

    this.notifyTime = moment(new Date()).format();

    this.chosenHours = new Date().getHours();
    this.chosenMinutes = new Date().getMinutes();

  }

  async checkNotifications(){
    let id = await this.localNotifications.getAll();
    console.log(id);
    //let scheduledNotifications = await this.localNotifications.getScheduled(id);
    //console.log(scheduledNotifications);
    let msg = id.length.toString();
    console.log(msg);
    let alert = await this.alertCtrl.create({
      header: 'Scheduled Notifications: ',
      message: msg,
      buttons: ['Ok']
  });

  await alert.present();
  }


  timeChange(time){
    this.chosenHours = time.hour.value;
    this.chosenMinutes = time.minute.value;
  }

  
  oneReminder(drinkAmount: number, unit:string){

    var msg = "Did you drink " + drinkAmount + " " + unit + " over the last hour?";
        var reminders = [{
          id: 11,
          text: msg, //defined above
          trigger: {at:  new Date(new Date().getTime()+(3600*1000*1))}
        }];
        this.localNotifications.schedule(reminders);
        console.log("Notifications to be scheduled: ", reminders);
  }

  twoReminders(drinkAmount: number, unit:string, time_amount:number){
    var milestone = drinkAmount/2;
    var msg = "Halfway there! Did you drink " + milestone + " " + unit + " yet?";
    var msg2 = "Did you drink " + drinkAmount + " " + unit + " over the last " + time_amount + " hours?";
        
    var reminders = [{
        id: 11,
        text: msg, //defined above
        trigger: {at:  new Date(new Date().getTime()+(3600*1000*time_amount/2))}
      },{
        id: 12,
        text: msg2, //defined above
        trigger: {at:  new Date(new Date().getTime()+(3600*1000*time_amount))}
      }];

    this.localNotifications.schedule(reminders);
    console.log("Notifications to be scheduled: ", reminders);
  }

  testReminder(waitTime){
    //wait time in hours
    var wait = new Date(new Date().getTime()+(3600*1000*waitTime));

    this.localNotifications.schedule({
      id: 9,
      text: 'Time to test again!',
      trigger: {at: wait}
    });

  }


  async cancelAll(){
    this.localNotifications.cancelAll();
  
    let alert = await this.alertCtrl.create({
        message: 'Notifications cancelled',
        buttons: ['Ok']
    });
  
    await alert.present();
  }

  testNotification(wait:number){
    var msg = "Here's a message";

    this.localNotifications.schedule({
      text: 'This is a Local Notification',
      trigger: {at: new Date(new Date().getTime() + wait)},
      led: 'FF0000'
   });
      
  }

  async addNotifications(days: any[], time: Date){

    //this.days
    //firstNotificationTime

    this.notifications = [];
    this.days = days;
    
    this.chosenHours = time.getHours();
    this.chosenMinutes = time.getMinutes();
  
    let currentDate = new Date();
    let currentDay = currentDate.getDay(); // Sunday = 0, Monday = 1, etc.
  
    let chosenDays = [];
    for(let day of this.days){
  
        if(day.checked){
          let dayId = day.dayCode;
          chosenDays.push(day.title);
            //let firstNotificationTime = new Date();
            //let dayDifference = day.dayCode - currentDay;
  
            /*
            if(dayDifference < 0){
                dayDifference = dayDifference + 7; // for cases where the day is in the following week
            }
            */
  
            //firstNotificationTime.setHours(firstNotificationTime.getHours() + (24 * (dayDifference)));
            //firstNotificationTime.setHours(this.chosenHours);
            //firstNotificationTime.setMinutes(this.chosenMinutes);
            //let week:string = 'week';

          let foo = await this.localNotifications.get(day.dayCode);
          console.log(foo);
          if(foo){
            console.log("Cancelling notification for "+day.dayCode);
            await this.localNotifications.cancel(day.dayCode);
          }
  
            let notification = {
                id: dayId,
                title: 'Test Reminder',
                text: 'Remember to check your hydration today!',
                trigger:{
                  count: 1,
                  every:{weekday:dayId, hour:this.chosenHours, minute:this.chosenMinutes}
                }
                //trigger:{every: week, firstAt: firstNotificationTime }               
                
            };
            console.log(notification);
            this.notifications.push(notification);

            this.localNotifications.schedule(notification);
        }
        else{
          let foo = await this.localNotifications.get(day.dayCode);
          console.log(foo);
          if(foo){
            console.log("Cancelling foo"+day.dayCode);
            await this.localNotifications.cancel(day.dayCode);
          }
        }
    }
    let alert = await this.alertCtrl.create({
      message: ('Notifications set for: '+chosenDays),
      buttons: ['Ok']
    });
    await alert.present();

    console.log("Notifications scheduled for: " + chosenDays);
    this.notifications.length = 0;


    /*
    if(this.platform.is('cordova')){
      await this.localNotifications.cancelAll();
      // Schedule the new notifications
      this.localNotifications.schedule(this.notifications);
      //let msg = this.notifications.toString;
    }
    */
  }

}
