import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Results } from '../models/results';
import { Insights } from '../models/insights';
import { NotifyService } from './notify.service';

@Injectable({
  providedIn: 'root'
})
export class InsightsService {

  

  insightsBundle:Insights = {bwl:0, hydration:0,hyd_factor:0,perceived_hyd:0, isOldTest:false, time:"ureaqa", uploadTime:"ureaqa", uid:"ureaqa", 
  sg:1, inverse: 0,newInverse: 0,newUreaqaScore:0, absRate:0, time_amount:0,drinkAmount:0,wait:0, unit:"L",hyd_desc: "ureaqa", hyd_msg: "ureaqa", 
  hasInsights:false, chartColor:"00afa0"  };
  
  constructor( public notify: NotifyService) { }

  hydrationInsights(r:Results): Insights{
    //hourly water absorbsion rate: 500 mL or 1 L in extreme heat
    //this.graph = false;
    let i = this.insightsBundle
    //existing stuff
    i.bwl = r.bwl;
    i.hydration = r.hydration;
    i.isOldTest = r.isOldTest;
    i.time = r.time;
    i.uploadTime = r.uploadTime;
    i.sg = r.sg;
    i.perceived_hyd = r.perceived_hyd;
    i.uid = r.uid;

    //new stuff
    i.inverse = 100-r.hydration; 
    i.absRate = 0.5;
    i.hyd_factor = i.bwl*i.inverse;
    i.newUreaqaScore = Math.round((200 - i.hyd_factor))/10;
    i.newInverse = 20-i.newUreaqaScore;
    i.hasInsights = true;
    i.wait = 0;
    
    if(i.bwl<1){ 
      //use mL
      i.bwl = i.bwl*1000;
      i.absRate = 500;
      i.unit = "mL";
    }
    else{
      i.absRate = 0.5;
      i.unit = "L";
    }

    

    if (i.hyd_factor <= 5){
      //fully hydrated
      i.hyd_desc = "Fully Hydrated";
      i.chartColor='#19B9A5';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      i.drinkAmount = i.bwl; //100 to 990 mL
      if (i.drinkAmount>10){
        i.drinkAmount = Math.round(i.drinkAmount/50)*50;//round to nearest 50 mL
      }
      i.time_amount = Math.round(i.drinkAmount/i.absRate) +1;
      if (i.time_amount<2){
        i.hyd_msg = "Drink over the next hour. Test again as needed.";
        //this.notify.oneReminder(i.drinkAmount, i.unit);

      }
      else{
        i.hyd_msg = "Drink over the next " + i.time_amount + " hours. Test again as needed.";
        //this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);

      }
    }
    else if(i.hyd_factor <= 20){
      //mild dehydration
      i.hyd_desc = "Hydrated";
      i.chartColor='#5AC89B';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      i.drinkAmount = i.bwl; //1 to 2 L
      i.time_amount = Math.round(i.drinkAmount/i.absRate) +1;
      if (i.drinkAmount<3){
        i.drinkAmount = Math.round(i.drinkAmount*10)/10;//round to nearest 100 mL
      }
      else{
        i.drinkAmount = Math.round(i.drinkAmount/50)*50;//round to nearest 50 mL
      }
      if (i.time_amount<2){
        i.hyd_msg = "Drink over the next hour. Test again as needed.";
        //this.notify.oneReminder(i.drinkAmount, i.unit);
      }
      else{
        i.hyd_msg = "Drink over the next " + i.time_amount + " hours. Test again as needed.";
        //this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);
        
      }
    }
    else if(i.hyd_factor <= 36){
      //moderate dehydration
      //1.4 to 2.5 L deficit
      i.hyd_desc = "Mildly Dehydrated";
      i.chartColor='#E6E688';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      i.drinkAmount = i.bwl*0.8; // 1 to 2 L
      i.time_amount = Math.round(i.drinkAmount/i.absRate) +1; //3 to 5 hours
      if (i.drinkAmount<5){
        i.drinkAmount = Math.round(i.drinkAmount*10)/10;//round to nearest 100 mL
      }
      else{
        i.drinkAmount = Math.round(i.drinkAmount/50)*50;//round to nearest 50 mL
      }
      i.hyd_msg = "Drink over the next " + i.time_amount + " hours. Test again after your next workout.";
      //this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);
    }
    else if(i.hyd_factor <= 56){
      //clinical dehydration
      //1.8 to 3.4 L deficit
      i.hyd_desc = "Moderately Dehydrated";
      i.chartColor='#FFEB84';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      i.drinkAmount = i.bwl*0.8; // 1.4 to 2.7 L
      i.time_amount = Math.round(i.drinkAmount/i.absRate) +1; //3 to 6 hours
      i.drinkAmount = Math.round(i.drinkAmount/100)*100;//round to nearest 100 mL
      
      i.hyd_msg = "Drink over the next " + i.time_amount + " hours. Test again at least 2 hours before your next workout.";
      //this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);
      
    }
    else if(i.hyd_factor <=80){
      //clinical dehydration
      i.hyd_desc = "Dehydrated";
      //2.3 - 4 L deficit
      i.chartColor='#FFC87D';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      i.drinkAmount = i.bwl*0.65; // 1.5 to 2.6 L
      i.time_amount = Math.round(i.drinkAmount/i.absRate); //3 to 5 hours
      if (i.drinkAmount<5){
        i.drinkAmount = Math.round(i.drinkAmount*10)/10;//round to nearest 100 mL
      }
      else{
        i.drinkAmount = Math.round(i.drinkAmount/100)*100;//round to nearest 100 mL
      }
      i.wait = i.time_amount*1.5;
      i.hyd_msg = "Drink over the next " + i.time_amount + " hours. Test again after " + i.wait + " hours.";
      //this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);
      //this.notify.testReminder(i.wait);
    }
    else if(i.hyd_factor <= 108){
      //clinical-severe dehydration
      i.hyd_desc = "Dehydrated";
      //2.7 - 4.2 L deficit
      i.chartColor='#FFA578';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      i.drinkAmount = i.bwl*0.65; // 1.7 to 2.7 L
      i.time_amount = Math.round(i.drinkAmount/i.absRate)-1; //2 to 4 hours
      if (i.drinkAmount<5){
        i.drinkAmount = Math.round(i.drinkAmount*10)/10;//round to nearest 100 mL
      }
      else{
        i.drinkAmount = Math.round(i.drinkAmount/100)*100;//round to nearest 100 mL
      }
      i.wait = i.time_amount*1.5;
      i.hyd_msg = "Drink over the next " + i.time_amount + " hours. Test again after " + i.wait + " hours.";
      //this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);
      //this.notify.testReminder(i.wait);

    }
    else if(i.hyd_factor <= 126){
      //severe dehydration
      i.hyd_desc = "Very Dehydrated";
      //3.8 - 5 L deficit
      i.chartColor='#FA8771';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      i.drinkAmount = i.bwl*0.5; // 1.9 to 2.5 L
      i.time_amount = Math.round(i.drinkAmount/i.absRate)-2; //2 to 3 hours
      if (i.drinkAmount<5){
        i.drinkAmount = Math.round(i.drinkAmount*10)/10;//round to nearest 100 mL
      }
      i.wait = i.time_amount+1;
      i.wait = 2.5;
      i.hyd_msg = "Drink over the next " + i.time_amount + " hours. Test again as soon as possible.";
      //this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);
      //this.notify.testReminder(2.5);
    }
    else if(i.hyd_factor <= 180){
      //severe dehydration
      i.hyd_desc = "Very Dehydrated";
      //4.5 - 6 L deficit
      i.chartColor='#FA7070';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      i.drinkAmount = i.bwl*0.5; // 2.2 to 3 L
      i.time_amount = Math.round(i.drinkAmount/i.absRate)-2; //2 to 4 hours
      if (i.drinkAmount<5){
        i.drinkAmount = Math.round(i.drinkAmount*10)/10;//round to nearest 100 mL
      }
      
      i.wait = 2.5;
      i.hyd_msg = "Drink over the next " + i.time_amount + " hours. Test again as soon as possible.";
      //this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);
      //this.notify.testReminder(2.5);

    }
    else{
      //SEVERE dehydration
      i.hyd_desc = "Severely Dehydrated";
      i.chartColor='#FA5050';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      i.drinkAmount = i.bwl; 
      i.time_amount = Math.round(i.drinkAmount/i.absRate) -2;
      if (i.drinkAmount<10){
        i.drinkAmount = Math.round(i.drinkAmount*10)/10;//round to nearest 100 mL
      }
      
      i.wait = 2.5;
      i.hyd_msg = "Are you feeling okay? Monitor your hydration closely over the next few hours.";
      //this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);
      //this.notify.testReminder(2.5);

    }
   /* //home page stuff
    this.doughnutChartData.length = 0;
    this.doughnutChartLabels.length = 0;
    this.doughnutChartColors = [{ 
      backgroundColor: [this.chartColor, "#ffffff"],
      hoverBackgroundColor: [this.chartColor, "#ffffff"],
      hoverBorderColor: ["#fff0","#fff0" ],
      hoverBorderWidth: [0,0],
      borderWidth: [0,0]
      }];

    console.log(this.doughnutChartColors);
    this.doughnutChartData = [this.ureaqaScore, this.inverseScore];
    this.doughnutChartLabels = ['Hydration', 'Dehydration'];
    setTimeout(()=>{this.graph = true}, 0);

    */
    /*
    this.toast.create({
      message: this.chartColor,
      duration: 3000,
      position: 'bottom'
    }).present();
    */
    //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
    console.log(i);
    return i;

  }
}
