import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Profile } from '../models/profile';
import { WeightEntry } from '../models/weightEntry';
import {InsightsService} from './insights.service';
import { Results } from '../models/results';
import { Insights } from '../models/insights';
import { NotifyService } from './notify.service';
//import { DocumentSnapshot } from '@google-cloud/firestore';


@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(public notify: NotifyService, public insights: InsightsService, public http: HttpClient, public db: AngularFirestore) {
   
  }


createProfile(username:string, firstName:string, lastName:string, email:string, id:string): Promise<void> {
  //const id = this.firestore.createId(); //might want to not do this

  const docRef = this.db.doc(`users/${id}`);

  docRef.get().subscribe((docSnapshot) =>{
    if (docSnapshot.exists) {
        // do stuff with the data
        return docRef.update({
          id,
          username,
          firstName,
          lastName,
          email,
        });

      
    } 
    else {
      // create the document
      return docRef.set({
        id,
        username,
        firstName,
        lastName,
        email,
      });
    }
  });

  return docRef.update({
    id,
    username,
    firstName,
    lastName,
    email,
  });
}

getProfileList(): AngularFirestoreCollection<any> {
  return this.db.collection(`users`);
}

getProfile(id:string): AngularFirestoreDocument<any> {
  console.log("Getting firestore doc: "+id);
  return this.db.doc(`users/${id}`);
}

getCurrentWeight(id:string): AngularFirestoreDocument<WeightEntry>{
  return this.db.doc(`users/${id}/weight/${id}`);
}


setCurrentWeight(id:string, weight_kg:Number): Promise<void> {
  //const docID = this.db.createId(); //might want to not do this
  let date = new Date();
  const timestamp = date.toISOString();
  return this.db.doc(`users/${id}/weight/${id}`).set({
    weight_kg,
    timestamp
  });
}

updateCurrentWeight(id:string, weight_kg:Number): Promise<void> {
  //const docID = this.db.createId(); //might want to not do this
  let date = new Date();
  const timestamp = date.toISOString();
  return this.db.doc(`users/${id}/weight/${id}`).update({
    weight_kg,
    timestamp
  });
}
getHydration(sg:number): number {
  const m:number =213.87;
  const b:number =214.87;

  sg=1+sg/1000;
  var bwPercent = sg*m-b;
  var hydration = Math.round(100-5*bwPercent);

  return hydration;
}

addTestResults(r:Results, weight_kg:number): Promise<void> {
const m:number =213.87;
const b:number =214.87;

r.sg=1+r.sg/1000;
var bwPercent = r.sg*m-b;
r.hydration = Math.round(100-5*bwPercent);
r.bwl = weight_kg*bwPercent/100;
var hyd_factor = r.bwl*Math.abs(100-r.hydration); // 0-180, this is the "hydration insights" number
var hyd_score = Math.round((200 - hyd_factor))/10; //0-20, this is the "new" ureaqa score

const docID = this.db.createId(); //might want to not do this


let i = this.insights.hydrationInsights(r);

if (i.time_amount<2){
  this.notify.oneReminder(i.drinkAmount, i.unit);
}
else{
  this.notify.twoReminders(i.drinkAmount, i.unit, i.time_amount);
  if(i.hyd_factor>56){
    this.notify.testReminder(i.wait);
  }    
  
}

return this.db.doc(`results/${docID}`).set({
    bwl: r.bwl,
    hydration: r.hydration,
    perceived_hyd: r.hydration,
    sg: r.sg,
    time: r.time,
    uid: r.uid,
    uploadTime:i.uploadTime,
    inverse: i.inverse,
    newUreaqaScore: i.newUreaqaScore,
    newInverse: i.newInverse,
    absRate: i.absRate,
    hyd_factor: i.hyd_factor,
    time_amount: i.time_amount,
    drinkAmount: i.drinkAmount,
    wait: i.wait,
    unit: i.unit,
    hyd_desc: i.hyd_desc,
    chartColor: i.chartColor,
    hyd_msg: i.hyd_msg,
    isOldTest:i.isOldTest,
    hasInsights:i.hasInsights,

  });
} 

getTestResults(uid:string): AngularFirestoreCollection<any> {
  //find this users 3 most recent results
  return this.db.collection(`results`, ref => ref.where("uid", "==", uid).orderBy("time", "desc").limit(3));
}

getSomeTestResults(uid:string): AngularFirestoreCollection<any> {
  //find this users 5 most recent results
  return this.db.collection(`results`, ref => ref.where("uid", "==", uid).orderBy("time", "desc").limit(5));
}
getWeeklyResults(uid:string, cutoff:String): AngularFirestoreCollection<any> {
  //find this users 10 most recent results
  var result: AngularFirestoreCollection<any> = this.db.collection(`results`, ref => ref.where("uid", "==", uid).where("time",">=",cutoff).orderBy("time").limit(10));
  return result;
}

getOneTest(docID:string): AngularFirestoreDocument<any>{
  return this.db.doc(`results/${docID}`);
}
/*
uploadImage(imageURL, uid):Promise<void>{
  return this.db.doc(`pictures/${uid}`).set({imageURL});
  //ionicthemes.com/tutorials/about/ionic-firebase-image-upload
}

getProfilePic(uid):AngularFirestoreDocument<Image>{
  
  return this.db.doc(`pictures/${uid}`);
   
}*/

}

