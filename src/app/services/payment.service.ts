import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from 'firebase';

declare var Stripe;
//import 'rxjs/add/operator/switchMap';
//import 'rxjs/add/operator/do';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  userId: string;
  membership: any;
  user: User;

  stripe = Stripe('pk_test_pwkzHMsOeJ5tYwi70fAchEsi');
  elements = this.stripe.elements();
  card = this.elements.create('card');

  constructor(private db: AngularFirestore, private afAuth: AngularFireAuth) {

    let self = this;
    let subAuth = this.afAuth.authState
      .subscribe(user => {
        this.user = user;
        this.userId = user.uid
        let docRef: Observable<any> = this.db.doc(`users/${user.uid}`).valueChanges();
        let sub = docRef.subscribe(res =>{
          if(res){

            res.forEach(doc => {
              this.membership = doc.proMembership;
            });
          }
        });
        if (!sub.closed){
          sub.unsubscribe();
        }
        if (!subAuth.closed){
          subAuth.unsubscribe();
        }
        
      });

  }

    processPayment(token: any) {
      return this.db.doc(`/users/${this.userId}`).update({ proMembership:{token: token.id }});
    }


    createStripeCustomer(){

      let user = this.user
      // register Stripe user
      return this.stripe.customers.create({
        email: user.email
      })
      .then(customer =>{
        /// update database with stripe customer id
        this.db.doc(`/customers/${customer.id}`).update({cusId: customer.id, uid:user.uid});
        this.db.doc(`/users/${user.uid}`).update({cusId: customer.id});
      })
    }
    
    
/*
    createSubscription(token){
      const tokenId = token;
      const userId  = this.userId;
        
      if (!tokenId) throw new Error('token missing');
      let obs:Observable<any> =  this.db.doc(`/users/${userId}`).valueChanges();
       obs.subscribe(res=>{
        let doc = res;
        this.stripe.subscriptions.create({
          customer: doc.cusId,
          source: tokenId,
          items: [
            {plan: 'Monthly - Early Adopter',},
          ],
        });
      })
       
      .then(sub => {
              admin.database()
              .ref(`/users/${userId}/pro-membership`)
              .update( {status: 'active'} )
      })
      .catch(err => console.log(err))
  
  });
    }
    */
    
}
