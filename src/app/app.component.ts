import { Component, NgZone } from '@angular/core';
import { Router} from '@angular/router';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';

import { AngularFireFunctions } from '@angular/fire/functions';
import { stringify } from '@angular/compiler/src/util';
import { Profile } from './models/profile';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  
  public appPages= [
    {title: 'Home', url: '/home' },
    {title: 'New Test', url: '/new-test'},
    {title: 'Insights', url: '/insights'},
    {title: 'Profile', url: '/profile'},
    {title: 'Create Profile', url: '/create-profile'}
  ];
  

//  @ViewChild(Nav) nav: Nav;
  //rootPage:any;
 
    
  constructor(
    private platform: Platform, private nav: NavController, private router: Router,
    private splashScreen: SplashScreen, private fun: AngularFireFunctions,
    private statusBar: StatusBar,private zone: NgZone,
    private afAuth: AngularFireAuth, private firebaseAnalytics: FirebaseAnalytics,
  ) {
    this.initializeApp();
  }

  initializeApp(){
    this.platform.ready().then(() => {
      this.afAuth.auth.onAuthStateChanged(async (user)=>{
        if (user){
          // User is signed in
          if(this.platform.is('android')) {
            this.statusBar.styleLightContent();
          }
          else{
            this.statusBar.styleDefault();
          }

          if(user.emailVerified){
            console.log("Creating Stripe customer");
            var userRecord:any = [{uid:"", email:""}];
            userRecord.uid = user.uid;
            userRecord.email = user.email;
            console.log(user.email);
            console.log(userRecord);
            const boi = await this.fun.httpsCallable('createExistingStripeCustomer')({email: user.email, uid:user.uid}).toPromise();
            console.log("Customer created");
            console.log(boi); 
          }
          
          this.splashScreen.hide();
          this.zone.run(async () =>{
            await this.router.navigateByUrl('/home');
          });
        }
        else{
          //no user is signed in
          if(this.platform.is('android')) {
            this.statusBar.styleLightContent();
          }
          else{
            this.statusBar.styleDefault();
          }
          this.splashScreen.hide();
          this.zone.run(async () =>{
            await this.router.navigateByUrl('/login');
          });
        }
  
      });

    });
  }
/*
  openPage(page) {
    if(this.platform.is('mobile')){
      //analytics
      this.firebaseAnalytics.logEvent('menu_nav_clicked',{Page: page.component})
          .then((res: any) => console.log(res))
          .catch((error: any) => console.error(error));
    }
    
    this.router.navigateByUrl(this.appPages.url);
  }*/

  logout(){
    /*
    this.firebaseAnalytics.logEvent('logout_button_clicked',{})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
*/
    var self = this;
    self.router.navigateByUrl('/login');  
    this.afAuth.auth.signOut().then(function() {
      console.log('Signed Out');
    }, function(error) {
      console.error('Sign Out Error', error);
    });
  }

}