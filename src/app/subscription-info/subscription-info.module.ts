import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { PaymentsModule } from 'src/app/payments/payments.module'

import { SubscriptionInfoPage } from './subscription-info.page';

const routes: Routes = [
  {
    path: '',
    component: SubscriptionInfoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentsModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [
    SubscriptionInfoPage
],
  declarations: [SubscriptionInfoPage]
})
export class SubscriptionInfoPageModule {}
