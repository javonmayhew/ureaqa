import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-subscription-info',
  templateUrl: './subscription-info.page.html',
  styleUrls: ['./subscription-info.page.scss'],
})
export class SubscriptionInfoPage implements OnInit {

  user:any;
  periodEnd: string;
  status:string;
  email:string;
  updatePayment:boolean = false;

  constructor(private modalCtrl: ModalController, private toast:ToastController, private auth: AngularFireAuth, private db: AngularFirestore) { }

  async ngOnInit() {
    console.log("Subscription Info Modal Initializing...");
    let userId = this.auth.auth.currentUser.uid;
    const userDoc = await this.db.doc(`users/${userId}`).get();
    let sub = userDoc.subscribe(doc=>{
      this.user = doc.data();
      console.log(this.user);
      this.periodEnd = new Date((this.user.periodEnd*1000)).toLocaleDateString();
      this.email = this.user.email;
      
      if (this.user.status){
        this.status = this.user.status;
        this.status = this.status.toLowerCase();
      }
      else{
        this.status = "trialling";
      }
      if (this.status !== "active"){
        this.updatePayment=true;
      }
    });
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  async presentToast() {
    const toast = await this.toast.create({
      message: 'Toast toasted',
      duration: 2000
    });
    toast.present();
  }

}
