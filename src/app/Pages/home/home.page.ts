import { Component, NgZone } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { NavController, ToastController, Platform } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { NotifyService } from '../../services/notify.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Observable, Subscription, Subject } from 'rxjs';
import { Results } from '../../models/results';
import { Router } from '@angular/router';
import { Insights } from 'src/app/models/insights';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage{

  testTime:Date;
  lastTestTime:Date;
  ureaqaScore:number = 0;
  inverseScore:number = 100-this.ureaqaScore;
  testList: Observable<any[]>;
  lastTest:Results;
  perceived_hyd:number;
  drinkAmount: number;
  hyd_msg: string;
  hyd_desc: string;
  chartColor: string;
  waterIntake:number;
  unit:string;
  uid:string;
  bwl:number;
  sg:number;
  showResults:boolean = true;
  unknownMsg: string = null;
  graph:boolean = false;
  doughnutChartColors: any[];
  mobilePlatform: string[];
  versionNumber: string;

  i:Insights = {bwl:0, hydration:0,hyd_factor:0,perceived_hyd:0, isOldTest:false, time:"ureaqa", uploadTime:"ureaqa", uid:"ureaqa", 
  sg:1, inverse: 0,newInverse: 0,newUreaqaScore:0, absRate:0, time_amount:0,drinkAmount:0,wait:0, unit:"L",hyd_desc: "ureaqa", hyd_msg: "ureaqa", 
  hasInsights:false, chartColor:"00afa0"  };

  // Doughnut
  public doughnutChartLabels:string[] = [];
  public doughnutChartData:number[] = [];
  public doughnutChartType:string = 'doughnut';
  
    //backgroundColor: ["#00afa0", "#ffffff"],
    //hoverBackgroundColor: ["#00afa0", "#ffffff"],
    //hoverBorderColor: ["#fff0","#fff0" ],
    //hoverBorderWidth: [0,0],
    //borderWidth: [0,0]
    
  public doughnutChartOptions: any = {
    legend: {
        display: false
    },
    tooltips: {
        displayColors:false,
        callbacks: {
            label : ((tooltipItem) => {
                let index = tooltipItem.index;
                let label = this.doughnutChartLabels[index];
                return label;
            })
        }
    }
  };
  


  constructor(public afProvider:FirestoreService, public notify: NotifyService, private afAuth: AngularFireAuth, private firebaseAnalytics: FirebaseAnalytics, 
    public navCtrl: NavController, private router: Router, private zone: NgZone, public toast: ToastController, public platform: Platform, private appVersion: AppVersion) {
  }
  
  
  private sub: Subscription = new Subscription();
  private subAuth: Subscription = new Subscription();

  ionViewWillEnter(){
    //this.sub.subscribe();
    //this.subAuth.subscribe();
    console.log("Home Page Will Enter");
    this.firebaseAnalytics.setCurrentScreen('HomePage');
    this.mobilePlatform = this.platform.platforms();
    this.appVersion.getVersionNumber()
    .then((res: string) => this.versionNumber = res)
    .catch((error: any) => console.error(error));
  }

  ionViewWillLeave(){
    console.log("Home Page Will Leave");
    if (!this.sub.closed){
      console.log("Unsubscribing from test results...");
      this.sub.unsubscribe();
    }
    if (!this.subAuth.closed){
      console.log("Unsubscribing from auth details...");
      this.subAuth.unsubscribe();
    }
        
  }

  ngOnDestroy(){
    console.log("Destroying Home Page");
  }

  ngOnInit() {
    console.log("Initializing Home Page");
    this.afAuth.auth.onAuthStateChanged((user)=>{
      if (!user){
        console.log("logging out...");
        // No user is signed in
        this.router.navigateByUrl('/login');
      }
      else{
        console.log("Logged in!");
      }
    });
    
    this.subAuth = this.afAuth.authState.subscribe(user => {
      console.log("Subscribing to auth details...");

      if(user && user.uid) {
        this.uid = user.uid;
        if (!this.subAuth.closed){
          console.log("Unsubscribing from auth details...");
          this.subAuth.unsubscribe();
        }
        else{
          console.log("Already unsubscribed from auth details!");
        }
        //this.i.hasInsights
        this.firebaseAnalytics.setUserId(this.uid);
        var testTime = new Date(0);
        var self = this;
        this.testList = this.afProvider.getTestResults(this.uid).valueChanges();
        this.sub = this.testList.subscribe(res =>{ 
          console.log("Subscribing to test results...");
          console.log(res);
          if (res&&res.length>1){
            this.showResults = true;

            res.forEach(function (doc){
              console.log("New Test:");
              console.log(doc.sg);
              var data= new Date(doc.time);
              var dateString = data.toISOString().split('T').shift();
              console.log(dateString);
              if (data.valueOf()>testTime.valueOf()) {
                testTime = data;
                self.lastTestTime = testTime;
                self.ureaqaScore = doc.hydration;
                self.inverseScore = 100-self.ureaqaScore;
                self.bwl = doc.bwl;
                self.lastTest = doc;

                if(doc.hasInsights!=null){
                  if(doc.hasInsights){
                    console.log("insights: ");
                    self.i = doc;
                    console.log(self.i);
                    self.updateChart();
                  }
                  else{
                    console.log("No Insights");
                    self.waterIntake = self.hydrationInsights(self.bwl, self.ureaqaScore);
                  }
                }
                else{
                  self.waterIntake = self.hydrationInsights(self.bwl, self.ureaqaScore);
                }
              }
            });

            console.log("Tests should be done");            
          }
          else{
            //No results yet
            this.showResults = false;
            this.doughnutChartData = [100, 0];
            this.unknownMsg = "N/A";
            this.ureaqaScore = null;
            this.doughnutChartData = [0, 100];
            this.hyd_msg = "Try adding a new test to see your personalized hydration plan.";
          }
          if (!this.sub.closed){
            console.log("Unsubscribing from test results...");
            this.sub.unsubscribe();
          }
          if (!this.subAuth.closed){
            console.log("Unsubscribing from auth details...");
            this.subAuth.unsubscribe();
          }
          else{
            console.log("Already unsubscribed from auth details!"); 
          }
          
          
        },error=>{
          console.error(error);
        });        


        //endif
        console.log("end if");

        //reroute thing. It's on different pages now
        /*
        var profile = this.afProvider.getProfile(this.uid).valueChanges();
        let statusCheck = profile.subscribe(async res =>{
          console.log("Subscribing to sub status...");
          let status = res.status;
          if (status !=="active"){
            this.router.navigateByUrl('/profile');

            const toast = await this.toast.create({
              message: "Check your subscription status.",
              duration: 3000,
              position: "middle"
            });
            toast.present();
          }
        });*/


      }
      else{
        console.log(`Could not find authentication details`);
      }

    }); 
  }

  newTest(){
    console.log('Button Clicked');
    
    this.zone.run(async () =>{
      await this.router.navigateByUrl('/new-test');
    });

    /*
    this.firebaseAnalytics.logEvent('new_test_nav_clicked',{uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
    */
    
  }

  updateChart(){

    this.hyd_desc = this.i.hyd_desc;
    this.hyd_msg = this.i.hyd_msg;
    this.ureaqaScore = this.i.hydration;
    this.waterIntake = this.i.drinkAmount;
    this.unit = this.i.unit;


    this.doughnutChartData.length = 0;
    this.doughnutChartLabels.length = 0;
    this.doughnutChartColors = [{ 
      backgroundColor: [this.i.chartColor, "#ffffff"],
      hoverBackgroundColor: [this.i.chartColor, "#ffffff"],
      hoverBorderColor: ["#fff0","#fff0" ],
      hoverBorderWidth: [0,0],
      borderWidth: [0,0]
      }];

    console.log(this.doughnutChartColors);
    this.doughnutChartData = [this.i.hydration, this.i.inverse];
    this.doughnutChartLabels = ['Hydration', 'Dehydration'];
    setTimeout(()=>{this.graph = true}, 0);
  }


  hydrationInsights(waterDeficit, ureaqaScore){
    //hourly water absorbsion rate: 500 mL or 1 L in extreme heat
    this.graph = false;
    var inverse = 100-ureaqaScore;
    var absRate :number  = 0.5;
    var hyd_factor = waterDeficit*inverse;
    var time_amount:number;
    var drinkAmount:number;    
    var reminders: any[];
    
    if(waterDeficit<1){ 
      //use mL
      waterDeficit = waterDeficit*1000;
      absRate = 500;
      this.unit = "mL";
    }
    else{
      absRate = 0.5;
      this.unit = "L";
    }

    var wait: number;

    if (hyd_factor <= 5){
      //fully hydrated
      this.hyd_desc = "Fully Hydrated";
      this.chartColor='#19B9A5';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      drinkAmount = waterDeficit; //100 to 990 mL
      if (drinkAmount>10){
        drinkAmount = Math.round(drinkAmount/50)*50;//round to nearest 50 mL
      }
      time_amount = Math.round(drinkAmount/absRate) +1;
      if (time_amount<2){
        this.hyd_msg = "Drink over the next hour. Test again as needed.";
        //this.notify.oneReminder(drinkAmount, this.unit);

      }
      else{
        this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again as needed.";
        //this.notify.twoReminders(drinkAmount, this.unit, time_amount);

      }
    }
    else if(hyd_factor <= 20){
      //mild dehydration
      this.hyd_desc = "Hydrated";
      this.chartColor='#5AC89B';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      drinkAmount = waterDeficit; //1 to 2 L
      time_amount = Math.round(drinkAmount/absRate) +1;
      if (drinkAmount<3){
        drinkAmount = Math.round(drinkAmount*10)/10;//round to nearest 100 mL
      }
      else{
        drinkAmount = Math.round(drinkAmount/50)*50;//round to nearest 50 mL
      }
      if (time_amount<2){
        this.hyd_msg = "Drink over the next hour. Test again as needed.";
        //this.notify.oneReminder(drinkAmount, this.unit);
      }
      else{
        this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again as needed.";
        //this.notify.twoReminders(drinkAmount, this.unit, time_amount);
        
      }
    }
    else if(hyd_factor <= 36){
      //moderate dehydration
      //1.4 to 2.5 L deficit
      this.hyd_desc = "Mildly Dehydrated";
      this.chartColor='#E6E688';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      drinkAmount = waterDeficit*0.8; // 1 to 2 L
      time_amount = Math.round(drinkAmount/absRate) +1; //3 to 5 hours
      if (drinkAmount<5){
        drinkAmount = Math.round(drinkAmount*10)/10;//round to nearest 100 mL
      }
      else{
        drinkAmount = Math.round(drinkAmount/50)*50;//round to nearest 50 mL
      }
      this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again after your next workout.";
      //this.notify.twoReminders(drinkAmount, this.unit, time_amount);
    }
    else if(hyd_factor <= 56){
      //clinical dehydration
      //1.8 to 3.4 L deficit
      this.hyd_desc = "Moderately Dehydrated";
      this.chartColor='#FFEB84';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      drinkAmount = waterDeficit*0.8; // 1.4 to 2.7 L
      time_amount = Math.round(drinkAmount/absRate) +1; //3 to 6 hours
      drinkAmount = Math.round(drinkAmount/100)*100;//round to nearest 100 mL
      
      this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again at least 2 hours before your next workout.";
      //this.notify.twoReminders(drinkAmount, this.unit, time_amount);
      
    }
    else if(hyd_factor <=80){
      //clinical dehydration
      this.hyd_desc = "Dehydrated";
      //2.3 - 4 L deficit
      this.chartColor='#FFC87D';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      drinkAmount = waterDeficit*0.65; // 1.5 to 2.6 L
      time_amount = Math.round(drinkAmount/absRate); //3 to 5 hours
      if (drinkAmount<5){
        drinkAmount = Math.round(drinkAmount*10)/10;//round to nearest 100 mL
      }
      else{
        drinkAmount = Math.round(drinkAmount/100)*100;//round to nearest 100 mL
      }
      wait = time_amount*1.5;
      this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again after " + wait + " hours.";
      //this.notify.twoReminders(drinkAmount, this.unit, time_amount);
      //this.notify.testReminder(wait);
    }
    else if(hyd_factor <= 108){
      //clinical-severe dehydration
      this.hyd_desc = "Dehydrated";
      //2.7 - 4.2 L deficit
      this.chartColor='#FFA578';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      drinkAmount = waterDeficit*0.65; // 1.7 to 2.7 L
      time_amount = Math.round(drinkAmount/absRate)-1; //2 to 4 hours
      if (drinkAmount<5){
        drinkAmount = Math.round(drinkAmount*10)/10;//round to nearest 100 mL
      }
      else{
        drinkAmount = Math.round(drinkAmount/100)*100;//round to nearest 100 mL
      }
      wait = time_amount*1.5;
      this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again after " + wait + " hours.";
      //this.notify.twoReminders(drinkAmount, this.unit, time_amount);
      //this.notify.testReminder(wait);

    }
    else if(hyd_factor <= 126){
      //severe dehydration
      this.hyd_desc = "Very Dehydrated";
      //3.8 - 5 L deficit
      this.chartColor='#FA8771';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      drinkAmount = waterDeficit*0.5; // 1.9 to 2.5 L
      time_amount = Math.round(drinkAmount/absRate)-2; //2 to 3 hours
      if (drinkAmount<5){
        drinkAmount = Math.round(drinkAmount*10)/10;//round to nearest 100 mL
      }
      wait = time_amount+1;
      this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again as soon as possible.";
      //this.notify.twoReminders(drinkAmount, this.unit, time_amount);
      //this.notify.testReminder(2.5);
    }
    else if(hyd_factor <= 180){
      //severe dehydration
      this.hyd_desc = "Very Dehydrated";
      //4.5 - 6 L deficit
      this.chartColor='#FA7070';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      drinkAmount = waterDeficit*0.5; // 2.2 to 3 L
      time_amount = Math.round(drinkAmount/absRate)-2; //2 to 4 hours
      if (drinkAmount<5){
        drinkAmount = Math.round(drinkAmount*10)/10;//round to nearest 100 mL
      }
      this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again as soon as possible.";
      //this.notify.twoReminders(drinkAmount, this.unit, time_amount);
      //this.notify.testReminder(2.5);

    }
    else{
      //SEVERE dehydration
      this.hyd_desc = "Severely Dehydrated";
      this.chartColor='#FA5050';
      //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
      drinkAmount = waterDeficit; 
      time_amount = Math.round(drinkAmount/absRate) -2;
      if (drinkAmount<10){
        drinkAmount = Math.round(drinkAmount*10)/10;//round to nearest 100 mL
      }
      this.hyd_msg = "Are you feeling okay? Monitor your hydration closely over the next few hours.";
      //this.notify.twoReminders(drinkAmount, this.unit, time_amount);

    }

    this.notify.testReminder(12);

   
    this.doughnutChartData.length = 0;
    this.doughnutChartLabels.length = 0;
    this.doughnutChartColors = [{ 
      backgroundColor: [this.chartColor, "#ffffff"],
      hoverBackgroundColor: [this.chartColor, "#ffffff"],
      hoverBorderColor: ["#fff0","#fff0" ],
      hoverBorderWidth: [0,0],
      borderWidth: [0,0]
      }];

    console.log(this.doughnutChartColors);
    this.doughnutChartData = [this.ureaqaScore, this.inverseScore];
    this.doughnutChartLabels = ['Hydration', 'Dehydration'];
    setTimeout(()=>{this.graph = true}, 0);
    /*
    this.toast.create({
      message: this.chartColor,
      duration: 3000,
      position: 'bottom'
    }).present();
    */
    //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
    return drinkAmount;

  }

}

