import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController, MenuController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Router} from '@angular/router';

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.page.html',
  styleUrls: ['./create-profile.page.scss'],
})
export class CreateProfilePage implements OnInit {
 

  slideOpts = {
    initialSlide: 0,
    
  };

  public createProfileForm: FormGroup;
  //router: any; //prolly need to fix this
  userId: string;
  email:string;

  constructor(public navCtrl: NavController, formBuilder: FormBuilder, private toastCtrl: ToastController,
    public firestoreProvider: FirestoreService, private afAuth: AngularFireAuth, private firebaseAnalytics: FirebaseAnalytics,
    public loadingCtrl: LoadingController, private router: Router, public menuCtrl: MenuController) {
      
      this.sub = this.afAuth.authState.subscribe(user => {
        if(user && user.uid){
          this.userId = user.uid;
          this.email = user.email;
        }        
      });


    this.createProfileForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      firstName: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      lastName: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      weight: ['', Validators.compose([Validators.required, Validators.min(75), Validators.max(300)])]
    });
  }
sub:any;
//put it inside of a provider?
  async createProfile(){

    const loading = await this.loadingCtrl.create();
    const username: string = this.createProfileForm.value.username;
    const firstName: string = this.createProfileForm.value.firstName;
    const lastName: string = this.createProfileForm.value.lastName;
    const email: string = this.email;
    const uid: string = this.userId;
    const weight_kg = this.createProfileForm.value.weight/2.20462;
    this.firestoreProvider.setCurrentWeight(uid, weight_kg);
    var user = this.afAuth.auth.currentUser;
    user.updateProfile({displayName: firstName, photoURL: null})
    .then(()=>{
    }, 
    (error)=>{
      console.log(error);
      this.presentToast("Something went wrong. Couldn't update profile.");
    });
    
    this.firestoreProvider.createProfile(username, firstName,lastName, email, uid).then(
      () => {
        loading.dismiss().then(() => {
          this.menuCtrl.enable(true);
          this.router.navigateByUrl('/profile');
        });
      },
      error => {
        console.error(error);
        loading.dismiss();
        this.presentToast("Verify your email to continue.");
        
      }
    );

    return await loading.present();
  } 

  async presentToast(msg:string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }
  ngOnDestroy(){
    //unsubscribe!!
    if(this.sub){
      this.sub.unsubscribe();
    }
  }

  ngOnInit() {
    
    console.log('ionViewDidLoad CreateProfilePage');
    this.firebaseAnalytics.setCurrentScreen('CreateProfilePage');

  }

}
