import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import {ChartsModule} from 'ng2-charts';
import { InsightsPage } from './insights.page';

const routes: Routes = [
  {
    path: '',
    component: InsightsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChartsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InsightsPage]
})
export class InsightsPageModule {}
