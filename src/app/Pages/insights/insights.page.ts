import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, Platform } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Observable, Subscription } from 'rxjs';
import { Results } from '../../models/results';
import { Router } from '@angular/router';
import { ChartsModule } from 'ng2-charts/ng2-charts';

@Component({
  selector: 'app-insights',
  templateUrl: './insights.page.html',
  styleUrls: ['./insights.page.scss'],
})
export class InsightsPage{

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  chart : [any];

  public barChartLabels:string[] = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
  public barChartLabelsRecent:string[] = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
  public barChartColors: any[] = [{ backgroundColor: "#00afa0", borderColor:"#0000"}, { backgroundColor: "#606060", borderColor:"#0000"}];

  public hydTestData = [];
  public pHydTestData = [];
  public hydData = [0,0,0,0,0,0,0];
  public pHydData = [0,0,0,0,0,0,0];
  public optHydData = [0,0,0,0,0,0,0];
  public testDay = [];
  testList: Observable<Results[]>;
  uid:string;
  hydMax:number;
  hydMin:number;
  hydAvg:number;
  pHydAvg:number;
  testCount:number;
  public displayOptions:any;
  public decision:string = "lastFive";
  public weeklyData:boolean = false;
  public showData:boolean = false;

  public barChartDataSet:any[] = [
    {data: [], label: 'Hydration Score'},
    {data: [0, 0, 0, 0, 0, 0, 0], label: 'Perceived Hydration'}
  ];
  public barChartData:any[] = [{data: []}];
    

  constructor(public afProvider:FirestoreService, private afAuth: AngularFireAuth, private firebaseAnalytics: FirebaseAnalytics, 
    private toast: ToastController,
    public navCtrl: NavController, private router: Router) {
      console.log("Constructing Insights Page");
      
  }

  private sub: Subscription = new Subscription();
  private subAuth: Subscription = new Subscription();

  ngOnInit(){
    console.log('Initializing Insights Page');
    this.subAuth = this.afAuth.authState
    .subscribe( 
      user => {
        console.log("Subscribing to auth details...");
        if(user&&user.uid) {
          this.uid = user.uid;
          this.displayOptions = "lastFive";
          if (this.uid){
            this.getRecentTests();
          }
          
          if (!this.subAuth.closed){
            console.log("Unsubscribing from auth details...");
            this.subAuth.unsubscribe();
          }
          else{
            console.log("Already unsubscribed from auth details!");
          }
        }  
        else{
          console.log(`Could not find authentication details`);
          this.router.navigateByUrl("/login");
        }
      },
      error => {
        console.log('Authentication Error');
        console.log(error);
      }  
    ) 
    
  }
  ngOnDestroy(){
    console.log("Destroying Insights Page ");
    this.sub.unsubscribe();
    this.subAuth.unsubscribe();
  }

  ionViewWillEnter() {
    console.log('Insights Page Will Enter');
    this.firebaseAnalytics.setCurrentScreen('InsightsPage');
  }

  ionViewWillLeave(){
    console.log("Insights Page Will Leave");
    if (!this.sub.closed){
      console.log("Unsubscribing from test results...");
      this.sub.unsubscribe();
    }
    if (!this.subAuth.closed){
      console.log("Unsubscribing from auth details...");
      this.subAuth.unsubscribe();
    }
        
  }

  makeDecision(){
    let newDecision = this.displayOptions;
    if(this.decision !== newDecision){
      //do something
      this.decision = newDecision;

      if(newDecision=="thisWeek"){
        this.weeklyData = true;
        this.getThisWeek();

      }
      else if(newDecision=="lastWeek"){
        this.weeklyData = true;
        this.getLastWeek();
      }
      else if(newDecision=="lastFive"){
        this.weeklyData = false;
        this.getRecentTests();
      }
    }
    else if(newDecision=="lastFive"){
      this.weeklyData = false;
      this.getRecentTests();
    }
  }

  doTheMath(){

    console.log("Starting Analysis");

    const hydMin = arr => Math.min(...arr);
    const hydMax = arr => Math.max(...arr);
    const hydAvg = (arr =>{ 
      if(arr){
        const val =  arr.reduce((a,b) => a + b, 0);
        return val;
      }
      else{
        return 0;
      }
    });

    
    this.hydMin = hydMin(this.hydTestData);
    this.hydMax = hydMax(this.hydTestData);
    this.hydAvg = Math.round(hydAvg(this.hydTestData)/this.testCount);
    this.pHydAvg = Math.round(hydAvg(this.pHydTestData)/this.testCount);
    console.log(this.hydAvg);
    console.log(this.pHydAvg);

    this.hydTestData.push(0);
    this.pHydTestData.push(0);



    //this.barChartDataSet = [{data: this.hydData, label: 'Hydration Score'},{data: this.pHydData, label: 'Perceived Hydration'}];

    if(this.hydAvg == 0){
      this.hydMin = 0;
      this.hydMax = 0;
      this.presentToast(`Could not find this week's test results`);
    }
    else{ //things are normal
        //this.sgMin = this.sgMin/1000 + 1;
        //this.sgMax = this.sgMax/1000 + 1;
        //this.sgAvg = this.sgAvg/1000 + 1;
    }

    console.log(this.hydData);
    console.log('Test Count: ' + this.testCount);
    console.log('Perceived Hydration: ' + this.pHydAvg);
    console.log('Average Hydration: ' + this.hydAvg);

  }

  getRecentTests(){
    console.log("get recent tests");
    this.showData = false;
    //this.barChartData.length = 0;
    this.barChartLabels.length = 0;
    this.hydData.length = 0;
    this.pHydData.length = 0;
    


    var self = this;
    var labels:any[] = [];
    console.log(this.uid);
    this.testList = this.afProvider.getSomeTestResults(this.uid).valueChanges();
    this.sub = this.testList.subscribe(res =>{ 
      console.log("Subscribing to test results...");
      console.log(res.length + " Results Found");
      if (res){
            self.testCount = res.length;
            let i = 0;
            let j = res.length - 1; //biggest array number is 4
            labels.length = 0;
            self.hydTestData.length = 0;
            self.pHydTestData.length = 0;
            labels.length = j;
            self.hydTestData.length = j;
            self.pHydTestData.length = j;

            res.forEach(function (doc){

              let k = j - i; //4, 3, 2, 1, 0
              var data = Math.round(doc.hydration);
              self.hydTestData[k]= data;
              var data2 = doc.perceived_hyd;
              self.pHydTestData[k]= data2;

              const months = ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
              
              var date= new Date(doc.time);
              var dateString = date.toISOString().split('T').shift();
              var d = new Date(dateString);

              let formatted_date = months[d.getMonth()] + "-" + d.getDate();
              console.log(formatted_date);

              labels[k] = formatted_date;             

              i=i+1;
              
            });
            this.hydData = this.hydTestData;
            this.pHydData = this.pHydTestData;

            this.doTheMath();
            
            this.barChartDataSet.length = 0;
            this.barChartLabels.length = 0;
            //this.barChartLabels.length = 0;
            setTimeout(()=>{
              //updating chart data
              this.barChartDataSet = [{data: this.hydTestData, label: 'Hydration Score'},{data: this.pHydTestData, label: 'Perceived Hydration'}];
              this.barChartLabels = labels;
              this.showData = true;

            }, 0);
            
            console.log("Tests should be done");
          }
          
          if (!this.sub.closed){
            console.log("Unsubscribing from test results...");
            this.sub.unsubscribe();
          }
          if (!this.subAuth.closed){
            console.log("Unsubscribing from auth details...");
            this.subAuth.unsubscribe();
          }
          else{
            console.log("Already unsubscribed from auth details!");
          }
      });
  }

  getThisWeek(){
    //get monday

    this.showData = false;
    //wipe it
    this.barChartDataSet.length = 0;
    this.barChartLabels.length = 0;
    this.hydData.length = 0;
    this.pHydData.length = 0;
    this.hydTestData.length = 0;
    this.pHydTestData.length = 0;

    this.hydData.length = 7;
    this.pHydData.length = 7;

    //initialize it
    this.hydData = [0,0,0,0,0,0,0];
    this.pHydData = [0,0,0,0,0,0,0];

    var labels = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];


    var testTime = new Date();
    var d=testTime;
    d = new Date(d);
    var day = d.getDay();
    var diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    testTime = new Date(d.setDate(diff)); //cutoff date is monday
    var cutoff: string = testTime.toISOString().split('T').shift();
    console.log(cutoff);

    
    var self = this;
    this.testList = this.afProvider.getWeeklyResults(this.uid, cutoff).valueChanges(); //Results after Monday
    this.sub = this.testList.subscribe(res =>{ 
      console.log("Subscribing to test results...");
      if (res){
        self.testCount = 0;
        console.log(res.length + " Results Found");
        res.forEach(function (doc, i){
          //save each specific gravity
          var data = Math.round(doc.hydration); 
          
          var data2 = doc.perceived_hyd;
          
          //self.testDay.push(doc.time);
          var t= new Date(doc.time);
          var weekcheck:number = t.valueOf()-testTime.valueOf();
          if (weekcheck < 605000000){
            var day = t.getDay();
            if (day == 0){
              //sunday
              day = 6;
            }
            else{
              //convert mon-sat
              day = day-1;
            }
            self.hydTestData.push(data);
            self.pHydTestData.push(data2);
            self.testCount= self.testCount + 1;
            //set Data to the appropriate day
            console.log(doc.time);
            console.log(day);
            if(self.hydData[day] == 0){
              self.hydData[day] = data;
              self.pHydData[day] = data2;

              self.optHydData[day] = 95;
              
              console.log("Adding data to day "+day);
              console.log(self.hydData);

            }
          
          }
      
        });
        console.log("Tests should be done");
        
      }
      else{
        //no results this week
      }
      if (!this.sub.closed){
        console.log("Unsubscribing from test results...");
        this.sub.unsubscribe();
      }
      if (!this.subAuth.closed){
        console.log("Unsubscribing from auth details...");
        this.subAuth.unsubscribe();
      }
      else{
        console.log("Already unsubscribed from auth details!");
      }        
     

      this.doTheMath();
                

      this.barChartDataSet.length = 0;
      this.barChartLabels.length = 0;

      setTimeout(()=>{
        //updating chart data
        this.barChartDataSet = [{data: this.hydData, label: 'Hydration Score'},{data: this.pHydData, label: 'Perceived Hydration'}];
        this.barChartLabels = labels;
        this.showData = true;

      }, 0);

    });  

    var profile = this.afProvider.getProfile(this.uid).valueChanges();
        let statusCheck = profile.subscribe(async res =>{
          console.log("Subscribing to sub status...");
          let status = res.status;
          if (status !=="active"){
            this.router.navigateByUrl('/profile');

            const toast = await this.toast.create({
              message: "Check your subscription status.",
              duration: 3000,
              position: "middle"
            });
            toast.present();
          }
        });

  }

  getLastWeek(){
    this.showData = false;
    //wipe it
    this.barChartDataSet.length = 0;
    this.barChartLabels.length = 0;
    this.hydData.length = 0;
    this.pHydData.length = 0;
    this.hydTestData.length = 0;
    this.pHydTestData.length = 0;

    //initialize it
    this.hydData = [0,0,0,0,0,0,0];
    this.pHydData = [0,0,0,0,0,0,0];
    var labels = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];

    //get monday
    var testTime = new Date();
    var d=testTime;
    d = new Date(d);
    var day = d.getDay();
    var diff = d.getDate() - 7 - day + (day == 0 ? -6:1); // go back 1 week and adjust when day is sunday
    testTime = new Date(d.setDate(diff)); //cutoff date is monday
    var cutoff: string = testTime.toISOString().split('T').shift();

    console.log(cutoff);

    
    var self = this;
    this.testList = this.afProvider.getWeeklyResults(this.uid, cutoff).valueChanges(); //Results after Monday
    this.sub = this.testList.subscribe(res =>{ 
      console.log("Subscribing to test results...");
      if (res){
        self.testCount = 0;
        console.log(res.length + " Results Found");
        res.forEach(function (doc, i){
          //save each specific gravity
          var data = Math.round(doc.hydration); 
          
          var data2 = doc.perceived_hyd;
          
          //self.testDay.push(doc.time);
          var t= new Date(doc.time);
          var weekcheck:number = t.valueOf()-testTime.valueOf();
          console.log(doc.time);
          console.log(weekcheck);
          if (weekcheck < 605000000){
            var day = t.getDay();
          
            if (day == 0){
              //sunday
             day = 6;
            }
            else{
              //convert mon-sat
              day = day-1;
            }
            //set Data to the appropriate day
            self.hydTestData.push(data);
            self.pHydTestData.push(data2);
            self.testCount = self.testCount + 1;
            if(self.hydData[day] == 0){
              self.hydData[day] = data;
              
              self.pHydData[day] = data2;
              

              self.optHydData[day] = 95;
              console.log("Adding data to day "+day);
              console.log(self.hydData);
            }
          }
        });
        console.log("Tests should be done");
      }
      if (!this.sub.closed){
        console.log("Unsubscribing from test results...");
        this.sub.unsubscribe();
      }
      if (!this.subAuth.closed){
        console.log("Unsubscribing from auth details...");
        this.subAuth.unsubscribe();
      }
      else{
        console.log("Already unsubscribed from auth details!");
      }
      

      this.doTheMath();

      this.barChartDataSet.length = 0;
      this.barChartLabels.length = 0;
      
      setTimeout(()=>{
        //updating chart data
        this.barChartDataSet = [{data: this.hydData, label: 'Hydration Score'},{data: this.pHydData, label: 'Perceived Hydration'}];
        this.barChartLabels = labels;
        this.showData = true;

      }, 0);
      

    });  

  }


  async presentToast(msg:string) {
    const toast = await this.toast.create({
      message: msg,
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }


}
