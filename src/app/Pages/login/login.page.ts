import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, Platform, MenuController, LoadingController } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../models/user';
import { Profile } from '../../models/profile';
import { Router } from '@angular/router';
import { AngularFireFunctions } from '@angular/fire/functions';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  LoginAttempt:boolean = false;
  user = {} as User;
  profile: Observable<Profile>;
  userId:string;
  private sub: Subscription = new Subscription();
  passwordReset: boolean = false;

  constructor(private afAuth:AngularFireAuth, private db: FirestoreService, private firebaseAnalytics: FirebaseAnalytics,
    private fun: AngularFireFunctions, private toastCtrl: ToastController, private loadingCtrl:LoadingController, private router: Router,
    public navCtrl: NavController, public menuCtrl: MenuController) {
  }

  ngOnInit(){
    console.log('Initializing Login Page...');
    this.firebaseAnalytics.setCurrentScreen('LoginPage');
    this.menuCtrl.enable(false);
    
  }

  ionViewWillLeave(){
    console.log("Login Page Will Leave");
    
    if (!this.sub.closed){
      console.log("Unsubscribing from test results...");
      this.sub.unsubscribe();
    }
        
  }

  ngOnDestroy(){
    //unsubscribe!!
    console.log('Destroying Login Page...');
    if(this.sub){
      this.sub.unsubscribe();
    }
  }
  loginAttempt(){
    this.LoginAttempt=true;
  }

  async login(user: User){
    let loader = await this.loadingCtrl.create({
      message: "Processing...",
      //duration: 30000
    });
    loader.present();
    user.email=user.email.trim();
    user.password=user.password.trim();
    try{
      const result = await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      console.log(result);  
      if (result.user.uid){
        console.log("User Id Exists");
        this.userId = result.user.uid;
        this.menuCtrl.enable(true);

        if(result.user.emailVerified){
          console.log("Email is Verified");
          //check if first login
          this.profile = this.db.getProfile(this.userId).valueChanges();
          console.log("I have the profile");
          console.log(this.profile);
          this.sub = this.profile.subscribe(async res =>{
            console.log("Subscribing to profile... ");
            console.log(res);
            if (res&&res.username.length>4){
              console.log("User Profile Exists");
              //profile exists
              console.log(res.username + " signed in");
              
              this.router.navigateByUrl('/home');
              
            }
            else{
              console.log("Need to Set Up Profile");
              //set up profile
              this.router.navigateByUrl('/create-profile');
            }
          }, e=>{
            console.log(e); 
          });
          console.log(this.sub);
          console.log("Done of that subscription! Moving on!");
        }
        else{
          console.log("Need to Verify Email");
          this.afAuth.auth.currentUser.sendEmailVerification().then(()=>{
            this.presentToast("Verify your email address first.");
          },(error) => {
            console.log(error);
            this.presentToast("Error creating profile.");
          });
        }
        console.log("Dismiss that loader");

        console.log("Dismiss that loader");
      loader.dismiss();
      }
      else{
        console.log("User Id Doesn't Exist");
        this.presentToast("Unknown login error");
        loader.dismiss();
      }    
    }
    catch(e){
      loader.dismiss();
      console.error(e);
      this.presentToast(e.message);
    }
    
  }

  register(){
    this.router.navigateByUrl('/register'); //push
  }

  resetPassword(){
    this.passwordReset = true;
    
  }

  sendPasswordEmail(email: string){
    email = email.trim();
    this.afAuth.auth.sendPasswordResetEmail(email)
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
    this.presentToast("Check your email to reset your password");
  }

  async presentToast(msg:string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }

}
