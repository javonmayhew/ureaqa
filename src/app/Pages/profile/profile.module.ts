import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilePage } from './profile.page';
import { NotificationSetupModalPageModule } from '../../notification-setup-modal/notification-setup-modal.module';
import { PaymentsModule } from 'src/app/payments/payments.module';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage 
  }
];

@NgModule({
  imports: [
    PaymentsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfilePage]
})
export class ProfilePageModule {}
