import { Component, OnInit } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version';
import { AlertController, NavController, ToastController, Platform } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Profile } from '../../models/profile';
import { WeightEntry } from '../../models/weightEntry';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { ModalController } from '@ionic/angular';
import { NotificationSetupModalPage } from '../../notification-setup-modal/notification-setup-modal.page';
import { SubscriptionInfoPage } from '../../subscription-info/subscription-info.page';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public updateWeightForm: FormGroup;
  userData: Observable<any>;
  public profile:Observable<any>;
  //public imageDoc:Observable<Image>;
  public weightEntry:Observable<WeightEntry>;
  avatarExists: boolean = false;
  showForm: boolean = false;
  currentWeight: number;
  weight_kg: number;
  kg_lbs = 2.20462;
  imageURL: string = "";


  /*
  notifyTime: any;
  notifications: any[] = [];
  days: any[];
  chosenHours: number;
  chosenMinutes: number;

*/

  public backgroundColor = "#555";
  
  
  public userId: string;
 
  constructor(formBuilder: FormBuilder, private afAuth: AngularFireAuth, private firebaseAnalytics: FirebaseAnalytics, 
    public localNotifications: LocalNotifications, private toast: ToastController, private router: Router,
    private afProvider: FirestoreService, public alertCtrl: AlertController, public modalController: ModalController,
    public navCtrl: NavController, public platform: Platform) {

      this.updateWeightForm = formBuilder.group({
        weight: ['', Validators.compose([Validators.required, Validators.min(50), Validators.max(500)])]
      });  


      


  }


  private sub: Subscription = new Subscription();
  private subAuth: Subscription = new Subscription();


  ionViewWillEnter() {
    console.log('ionViewDidLoad ProfilePage');
    this.firebaseAnalytics.setCurrentScreen('ProfilePage');
  }

  ionViewWillLeave(){
    console.log("Profile Page Will Leave");
    if (!this.sub.closed){
      console.log("Unsubscribing from test results...");
      this.sub.unsubscribe();
    }
    if (!this.subAuth.closed){
      console.log("Unsubscribing from auth details...");
      this.subAuth.unsubscribe();
    }
        
  }

  ngOnInit() {
    console.log("Initializing New Test Page");
    this.subAuth = this.afAuth.authState.subscribe(user => {
      console.log("Subscribing to auth details...");
      if(user&&user.uid) {
        this.userId = user.uid;  
        if (!this.subAuth.closed){
          console.log("Unsubscribing from auth details...");
          this.subAuth.unsubscribe();
        }
        else{
          console.log("Already unsubscribed from auth details!");
        }
        this.profile = this.afProvider.getProfile(this.userId).valueChanges();
        //const imgRef: AngularFirestoreDocument<Image> = this.afProvider.getProfilePic(this.userId);
        //imgRef.valueChanges().subscribe(res=>{if(res){this.imageURL = res.imageURL;}else{this.imageURL="";}}); 
        //if(this.imageURL.length > 1){this.avatarExists = true;}
        this.weightEntry = this.afProvider.getCurrentWeight(this.userId).valueChanges();
        this.sub = this.weightEntry.subscribe(res =>{
          console.log("Subscribing to weight results...");
          this.weight_kg = res.weight_kg;
          this.currentWeight = Math.round(this.weight_kg*this.kg_lbs);
          if (!this.sub.closed){
            console.log("Unsubscribing from weight results...");
            this.sub.unsubscribe();
          }
          if (!this.subAuth.closed){
            console.log("Unsubscribing from auth details...");
            this.subAuth.unsubscribe();
          }
          else{
            console.log("Already unsubscribed from auth details!");
          }
        });
      }
      else{
        console.log(`Could not find authentication details`);
        this.router.navigateByUrl("/login");
      }
    }); 
  }





  ngOnDestroy(){
    console.log("Destroying Home Page");
  }

  toggleForm(){
    this.showForm = !this.showForm;

  }

  async presentToast(msg:string) {
    const toast = await this.toast.create({
      message: msg,
      duration: 1000,
    });
    toast.present();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: NotificationSetupModalPage
    });
    return await modal.present();
  }

  async subModal(){
    const modal = await this.modalController.create({
      component: SubscriptionInfoPage
    });
    return await modal.present();
  }

  updateWeight(){
    this.currentWeight = this.updateWeightForm.value.weight;
    this.weight_kg= this.currentWeight/this.kg_lbs;
    this.afProvider.updateCurrentWeight(this.userId, this.weight_kg);
    this.presentToast(`Weight Updated`);
    this.showForm = !this.showForm;
  }
/*
  openImagePickerCrop(){
    this.imagePicker.hasReadPermission().then(
      (result) => {
        if(result == false){
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        }
        else if(result == true){
          this.imagePicker.getPictures({
            maximumImagesCount: 1
          }).then(
            (results) => {
              for (var i = 0; i < results.length; i++) {
                this.crop.crop(results[i], {quality: 75}).then(
                  newImage => {
                    this.uploadImageToFirebase(newImage);
                  },
                  error => console.error("Error cropping image", error)
                );
              }
            }, (err) => console.log(err)
          );
        }
      }, (err) => {
        console.log(err);
      });
    }
*/

    /*
  uploadImageToFirebase(image){
    image = normalizeURL(image);
  
    //uploads img to firebase storage
    this.afProvider.uploadImage(image, this.userId)
    //this is all unnecessary
    .then(photoURL => {
  
      let toast = this.toast.create({
        message: 'Image was updated successfully',
        duration: 3000
      });
      toast.present();
      })
    }

    getAvatar():String{
      if(this.imageURL.length > 1){
        this.avatarExists = true;
      }
      else{
        this.avatarExists = false;
      }
      return this.imageURL;
    }
  */

  


}





