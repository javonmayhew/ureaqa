import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { User } from '../../models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  message:string;
  user = {} as User;
  email: string;
  password: string;

  constructor(private afAuth: AngularFireAuth, private firebaseAnalytics: FirebaseAnalytics, 
    private toastCtrl: ToastController, public loadingCtrl: LoadingController, private router: Router,
    public navCtrl: NavController) {
  }

  ngOnInit() {
  }
  async register(user:User){
    this.email = user.email.trim();
    this.password = user.password.trim();
    try{
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.password);
      console.log(result);
      if (result){
        

        try{
          //login my dude
          const login = await this.afAuth.auth.signInWithEmailAndPassword(this.email, this.password);
          console.log(login);  
          if (login && result.additionalUserInfo.isNewUser){
            //this wasn't working because of the 2 queries
            this.router.navigateByUrl('/create-profile'); //set up profile          
            
          }
          else{
            this.presentToast("Something went wrong");
            this.navCtrl.pop();
          }    
        }
        catch(e1){
          console.error(e1);
          this.presentToast(e1.message);
        }    
      }
      else{
        this.presentToast("Unknown login error.");
      }          
    }
    catch(e){
      console.error(e);
      this.presentToast(e.message);

    }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
    this.firebaseAnalytics.setCurrentScreen('RegisterPage');
  }

  async presentToast(msg:string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }
  

}
