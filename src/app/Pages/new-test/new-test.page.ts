import { Component, OnInit } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { NavController, LoadingController, ToastController, Platform, AlertController } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Observable, Subscription } from 'rxjs';
import { WeightEntry } from '../../models/weightEntry';
import { Router } from '@angular/router';
import { Results } from 'src/app/models/results';

@Component({
  selector: 'app-new-test',
  templateUrl: './new-test.page.html',
  styleUrls: ['./new-test.page.scss'],
})
export class NewTestPage implements OnInit {
  public weightEntry:Observable<WeightEntry>;
  Hydration: number = 80;
  perceived_hyd: number = 90;
  sg: number = 15;
  weight_kg = 100;
  disSG: string = "1.015";
  hydMsg: string = "Mildly dehydrated.";
  desMsg: string = "My performance won't be affected.";
  newTest: boolean = false;
  oldTest: boolean = false;
  preTest: boolean = true;
  myDate: string;
  myTime: string;
  testTime: string;
  uploadTime: string;
  uid: string;
  mobilePlatform: string[];
  versionNumber: string;
  loadingModalDisplayed: boolean;
  results: Results = {bwl:0, hydration: 100, perceived_hyd:100, sg: 1.005, time:"undefined", uploadTime:"undefined",uid:"undefined", isOldTest:false };
  

  

  constructor(public afProvider: FirestoreService, private toast: ToastController, private router: Router,
    private afAuth: AngularFireAuth, public firestoreProvider: FirestoreService,  private firebaseAnalytics: FirebaseAnalytics,
    public loadingCtrl: LoadingController, public alertController: AlertController, public navCtrl: NavController, public platform: Platform, private appVersion: AppVersion) {
     
  }

  private sub: Subscription = new Subscription();
  private subAuth: Subscription = new Subscription();

  
  ionViewWillEnter() {
    console.log('New Test Page Will Enter');
    this.firebaseAnalytics.setCurrentScreen('NewTestPage');
    this.mobilePlatform = this.platform.platforms();
    
    this.appVersion.getVersionNumber()
    .then((res: string) => this.versionNumber = res)
    .catch((error: any) => console.error(error));

    this.firebaseAnalytics.logEvent('new_test_screen_loads',{uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
  }

  ionViewWillLeave(){
    console.log("New Test Page Will Leave");
    if (!this.sub.closed){
      console.log("Unsubscribing from weight results...");
      this.sub.unsubscribe();
    }
    if (!this.subAuth.closed){
      console.log("Unsubscribing from auth details...");
      this.subAuth.unsubscribe();
    }
        
  }


  ngOnInit() {
    console.log("Initializing New Test Page");
    this.subAuth = this.afAuth.authState.subscribe(user => {
      console.log("Subscribing to auth details...");
      if(user&&user.uid) {
        this.uid = user.uid;  
        if (!this.subAuth.closed){
          console.log("Unsubscribing from auth details...");
          this.subAuth.unsubscribe();
        }
        else{
          console.log("Already unsubscribed from auth details!");
        }
        this.weightEntry = this.afProvider.getCurrentWeight(this.uid).valueChanges();
        this.sub = this.weightEntry.subscribe(res =>{
          console.log("Subscribing to weight results...");
          this.weight_kg = res.weight_kg;
          if (!this.sub.closed){
            console.log("Unsubscribing from weight results...");
            this.sub.unsubscribe();
          }
          if (!this.subAuth.closed){
            console.log("Unsubscribing from auth details...");
            this.subAuth.unsubscribe();
          }
          else{
            console.log("Already unsubscribed from auth details!");
          }
        });
      }
      else{
        console.log(`Could not find authentication details`);
        this.router.navigateByUrl("/login");
      }

      var profile = this.afProvider.getProfile(this.uid).valueChanges();
        let statusCheck = profile.subscribe(async res =>{
          console.log("Subscribing to sub status...");
          let status = res.status;
          if (status !=="active"){
            this.router.navigateByUrl('/profile');

            const toast = await this.toast.create({
              message: "Check your subscription status.",
              duration: 3000,
              position: "middle"
            });
            toast.present();
          }
        });
    }); 
  }

  ngOnDestroy(){
    console.log("Destroying New Test Page");
  }

  updateMsg(){
    switch (this.perceived_hyd){
      case 70:
        this.hydMsg = "Severely dehydrated.";
        this.desMsg = "I'm not feeling well.";
        break;
      case 80:
        this.hydMsg = "Dehydrated.";
        this.desMsg = "I'm not quite ready to perform.";
        break;
      case 90:
        this.hydMsg = "Mildly dehydrated.";
        this.desMsg = "My performance won't be affected.";
        break;
      case 100:
        this.hydMsg = "Fully hydrated!";
        this.desMsg = "I'm ready to perform.";
        break;
      case 110:
        this.hydMsg = "Over-hydrated.";
        this.desMsg = "I'm feeling bloated.";
        break;
      default:
        this.hydMsg = "Error: hydration unknown.";
        this.desMsg = "Something went wrong.";        
    }
  }


  updateSG(){
    this.disSG = (this.sg/1000 + 1).toFixed(3);
    }

  startTest(){
    this.preTest=false;
    this.newTest=true;


    
    this.firebaseAnalytics.logEvent('start_test_button_pressed',{uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
    this.firebaseAnalytics.logEvent('either_test_button_pressed',{uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
    
    this.presentAlertConfirm(); 
  }

  addResults(){    
    this.preTest=false;
    this.oldTest=true;
  
    this.firebaseAnalytics.logEvent('previous_results_button_pressed',{uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
    this.firebaseAnalytics.logEvent('either_test_button_pressed',{uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
    
  }


  //see how this works
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Hold Up!',
      message: 'Wait 30s before checking results',
      buttons: [
        {
          text: 'I Already Did',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
            this.loadingModalDisplayed = false;
          }
        }, {
          text: 'Start Timer',
          handler: () => {
            console.log('Confirm Okay');
            this.loadingModalDisplayed = true;
            this.presentLoading();
          }
        }
      ]
    });

    await alert.present();
  }

 //backdropDismiss: true,
  async presentLoading() {
    const loader = await this.loadingCtrl.create({
      message: "Please wait 30s...",
      duration: 30000
    });
    return await loader.present();

    //this shit broke

    /* 
    //ionLoadingWillPresent
    var waitTime: number = new Date().getMilliseconds();
    this.firebaseAnalytics.logEvent('modal_displayed',{uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));


    //ionLoadingDidDissmiss
    loader.onDidDismiss(() => {
      var newTime: number = new Date().getMilliseconds();
      waitTime = newTime - waitTime;
      this.firebaseAnalytics.logEvent('modal_dismissed',{Wait_Time: waitTime, uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));
      console.log('Dismissed loading');
      console.log('Wait Time: ' + waitTime + ' ms');
    });
    */
    
  }

  reset(){
    this.preTest=true;
    this.oldTest=false;
    this.newTest=false;
  }

  async updateDB(){
    this.Hydration = this.firestoreProvider.getHydration(this.sg);
    var diff = this.perceived_hyd - this.Hydration;
    this.firebaseAnalytics.logEvent('add_results_button_pressed',{Is_Old_Test: this.oldTest, Perception_Difference: diff, SG_Reading: this.sg, uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));

    //return current time
    let date = new Date();
    this.testTime = date.toISOString();
    console.log(this.testTime);
    this.results.uploadTime = this.testTime;
    console.log(this.results.uploadTime);


    if(this.oldTest){
      //return test time
      //need to get utc offset
      this.results.isOldTest = true;
      if (this.myDate.includes("undefined")){
        this.presentToast("Please enter the date of the test and try again");
        return;
      }
      if (this.myTime.includes("undefined")){
        this.presentToast("Please enter the time of the test and try again");
        return; //stop test
      }
      this.testTime = this.myDate +"T"+ this.myTime + 'Z';

      this.firebaseAnalytics.logEvent('old_results_added',{Is_Old_Test: this.oldTest, Perception_Difference: diff, SG_Reading: this.sg, uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error)); 
      
    }
    else{
      this.results.isOldTest = false;
      
      this.firebaseAnalytics.logEvent('new_results_added',{Loading_Modal_Displayed: this.loadingModalDisplayed, Is_Old_Test: this.oldTest, Perception_Difference: diff, SG_Reading: this.sg, uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber})
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));
      
    }

    console.log("Adding Results...");
    const loading = await this.loadingCtrl.create();
    this.results.uid = this.uid;
    this.results.sg = this.sg;
    this.results.time = this.testTime;
    this.results.perceived_hyd = this.perceived_hyd;
    this.results.bwl = 0;
    this.results.hydration = 100;

    this.firestoreProvider.addTestResults(this.results, this.weight_kg).then(
     () => {
     loading.dismiss().then(() => {
       this.presentToast('Success!');
       console.log("Results added!");
       this.router.navigateByUrl('/home');
      });
      },
      error => {
      console.error(error);
      }
    );
  

    return await loading.present();
  } 
 
  async presentToast(msg:string) {
    const toast = await this.toast.create({
      message: msg,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  

}

