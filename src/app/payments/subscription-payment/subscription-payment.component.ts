import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import {LoadingController, ToastController, AlertController, ModalController} from '@ionic/angular';
import { environment } from '../../../environments/environment';
import { PaymentService } from '../../services/payment.service';
import {AngularFireFunctions} from '@angular/fire/functions'
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoadedRouterConfig } from '@angular/router/src/config';
import { Router } from '@angular/router';

declare var Stripe: any;

//const stripe = Stripe('pk_test_pwkzHMsOeJ5tYwi70fAchEsi');
const stripe = Stripe('pk_live_FfXHwxhjd007yLqp2U6VIRIa');
const elements = stripe.elements();

var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

const card = elements.create('card', {style: style});


@Component({
  selector: 'app-subscription-payment',
  templateUrl: './subscription-payment.component.html',
  styleUrls: ['./subscription-payment.component.scss']
})
export class SubscriptionPaymentComponent implements OnInit, AfterViewInit {
  //@ViewChild('cardElement') cardElement: ElementRef;
 
  user:any;
  plan:string = "plan_Edy3Rb42tannPg";
  name:string;
  email:string;
  phoneNumber:string;
  address:string;
  city:string;
  province:string;
  postalCode:string;
  country:string = "CA";
  coupon:string;
  discount:any = {discountId:"", message:"", planId: ""};

  constructor(public pmt: PaymentService,  public loadingCtrl: LoadingController, public toastCtrl: ToastController, private modalCtrl: ModalController,
    private fun: AngularFireFunctions, private auth: AngularFireAuth, private db: AngularFirestore, private router: Router, public alertController: AlertController) { }

  ngAfterViewInit(){
    card.mount('#cardElement');//this.cardElement.nativeElement
  }
  

  async ngOnInit() {
    let self = this;
    console.log("initializing subscription payment");
    let userId = this.auth.auth.currentUser.uid;
    const userDoc = await this.db.doc(`users/${userId}`).get();
    let sub = userDoc.subscribe(doc=>{
      this.user = doc.data();
      console.log(this.user);
      //this.name = this.user.firstName + " " + this.user.lastName;
      //this.email = this.user.email;

    });
  }

  // Form submission Event Handler
  async handleForm(e) {
    e.preventDefault();
    this.presentLoading();
    let loader = await this.loadingCtrl.create({
      message: "Processing...",
      duration: 10000
    });
    loader.present();

    if (this.country=="CA"){//switch to CAD
      if (this.plan == "plan_FqC1c5XXho1MVy"){//monthly
        this.plan = "plan_FqC0vrxS19EHGd";
      }
      else{ //annual
        this.plan = "plan_Edy7npHOJ4EebQ";
      }
    }
    this.user.email = this.email;
    this.user.plan = this.plan;
    this.user.name = this.name;
    this.user.phoneNumber = this.phoneNumber;
    this.user.address = this.address;
    this.user.city = this.city;
    this.user.province = this.province;
    this.user.postalCode = this.postalCode;
    this.user.country = this.country;

    const { token, error } = await stripe.createToken(card);

    if (error) {
      // Inform the customer that there was an error.
      const errorElement = document.getElementById('card-errors');
      errorElement.textContent = error.message;
    } else {
      try {
        const res = await this.fun
        .httpsCallable('startSubscription')({ source: token.id, user: this.user}) //discount: this.discount 
        .toPromise();
        console.log(res);
        if (res){
          console.log("Presenting alert now");
          this.presentAlert();

          

        }
      } catch (error) {
        console.error(error);
        this.toastCtrl.create({message: "Something went wrong. Try again, or contact Ureaqa Support for help."});
        loader.dismiss();
      }


     
    }
    loader.dismiss();
    this.toastCtrl.create({message: "Settings Updated"});
    console.log("Settings Updated");
    
  }

  async presentLoading(){
    
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Payment Succeeded',
      message: 'Your payment succeeded. You can check your status anytime from your profile page.',
      buttons: [
        {
            text: 'OK',
            handler: () => {
              this.router.navigateByUrl('/profile');
              this.modalCtrl.dismiss();
            }
        }
    ]

    });

    await alert.present();
      
    
  }

  async checkDiscount(){
    const userDoc = await this.db.doc(`discounts/${this.coupon}`).get();
    let sub = userDoc.subscribe(async doc=>{
      if (doc.exists){
        this.discount = doc.data();
        console.log(this.discount);
        console.log(this.discount.planId.exists); //see if this works
        if(this.discount.planId.exists && this.discount.planId.startsWith("plan")){
          this.plan = this.discount.planId;
          console.log(this.plan);
        }
        
        const toast = await this.toastCtrl.create({
          message: this.discount.message,
          duration: 2000
        });
        toast.present();
      }
      else{
        this.discount = {discountId:"", message:""};
        console.log(this.discount);

        const toast = await this.toastCtrl.create({
          message: "We couldn't find that coupon.",
          duration: 2000
        });
        toast.present();
      }
      

    });
  }

}


