import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionPaymentComponent } from './subscription-payment/subscription-payment.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [SubscriptionPaymentComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  exports: [SubscriptionPaymentComponent]
})
export class PaymentsModule { }
