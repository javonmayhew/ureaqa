import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, NavParams, IonNav } from '@ionic/angular';

import * as moment from 'moment';
import { NotifyService } from '../services/notify.service';

@Component({
  selector: 'app-notification-setup-modal',
  templateUrl: './notification-setup-modal.page.html',
  styleUrls: ['./notification-setup-modal.page.scss'],
})
export class NotificationSetupModalPage implements OnInit {

  time: Date;
  notifyTime: any;
  notifications: any[] = [];
  days: any[]; 
  chosenHours: number;
  chosenMinutes: number;

  constructor(private modalCtrl: ModalController, private notify: NotifyService, private toast:ToastController) { 
    
    this.notifyTime = moment(new Date()).format();
    this.time= new Date(this.notifyTime);
    this.chosenHours = new Date().getHours();
    this.chosenMinutes = new Date().getMinutes();

    this.days = [
      {title: 'Monday', dayCode: 1, checked: false},
      {title: 'Tuesday', dayCode: 2, checked: false},
      {title: 'Wednesday', dayCode: 3, checked: false},
      {title: 'Thursday', dayCode: 4, checked: false},
      {title: 'Friday', dayCode: 5, checked: false},
      {title: 'Saturday', dayCode: 6, checked: false},
      {title: 'Sunday', dayCode: 0, checked: false}
    ];
    
  }

  ngOnInit() {
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  timeChange(){
    this.time = this.notifyTime;
    console.log(this.time);
    this.time = new Date(this.notifyTime);
    console.log(this.time);
}

  cancelAll(){
    console.log("Cancel all");
    this.notify.cancelAll();
  }

  testNotification1(){
    this.notify.testNotification(5000); //5 seconds
  }

  testNotification2(){
    this.notify.testNotification(300000); // 5 minutes
  }

  testCheckNotifications(){
    this.notify.checkNotifications(); //display alert with scheduled notifications
    this.presentToast();
  }

  async presentToast() {
    const toast = await this.toast.create({
      message: 'Notifications Checked',
      duration: 2000
    });
    toast.present();
  }
  
  
  addNotifications(){
    console.log("Add things");
    console.log(this.days);
    console.log(this.time);
    this.notify.addNotifications(this.days,this.time);
    this.closeModal();
  }

}
