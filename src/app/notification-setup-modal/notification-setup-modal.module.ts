import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NotificationSetupModalPage } from './notification-setup-modal.page';

const routes: Routes = [
  {
    path: '',
    component: NotificationSetupModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],

  entryComponents: [
    NotificationSetupModalPage
],
  declarations: [NotificationSetupModalPage]
})
export class NotificationSetupModalPageModule {}
