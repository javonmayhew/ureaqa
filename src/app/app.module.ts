import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationSetupModalPageModule } from './notification-setup-modal/notification-setup-modal.module';
import { NotificationSetupModalPage } from './notification-setup-modal/notification-setup-modal.page';
import { SubscriptionInfoPageModule } from './subscription-info/subscription-info.module';
import { SubscriptionInfoPage } from './subscription-info/subscription-info.page';
import { HttpClientModule } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import {ChartsModule} from 'ng2-charts';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FIREBASE_CONFIG } from './app.firebase.config';
import { FirestoreService } from './services/firestore.service';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [NotificationSetupModalPage, SubscriptionInfoPage],
  imports: [
    BrowserModule, BrowserAnimationsModule,  HttpClientModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    ChartsModule,
    FormsModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireFunctionsModule,
    NotificationSetupModalPageModule,
    SubscriptionInfoPageModule

  ],
  providers: [
    StatusBar,
    SplashScreen,
    LocalNotifications,
    AngularFirestore,
    FirebaseAnalytics,
    AppVersion,
    FirestoreService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

/*
IonicModule.forRoot(MyApp, {
  menuType: 'push',
  platforms: {
    ios: {
      menuType: 'overlay',
    }
  }
}),*/