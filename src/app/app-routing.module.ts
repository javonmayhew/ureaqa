import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'insights', loadChildren: './pages/insights/insights.module#InsightsPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  { path: 'new-test', loadChildren: './pages/new-test/new-test.module#NewTestPageModule' },
  { path: 'create-profile', loadChildren: './pages/create-profile/create-profile.module#CreateProfilePageModule' },
  { path: 'notification-setup-modal', loadChildren: './notification-setup-modal/notification-setup-modal.module#NotificationSetupModalPageModule' },
  { path: 'subscription-info', loadChildren: './subscription-info/subscription-info.module#SubscriptionInfoPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
