export interface Results {
    bwl: number;
    hydration:number;
    perceived_hyd:number;
    sg:number;
    time:string;
    uploadTime:string;
    uid: string;
    isOldTest:boolean;
}