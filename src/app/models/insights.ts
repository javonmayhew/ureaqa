export interface Insights {
    time: string;
    uploadTime:string;
    perceived_hyd: number;
    sg: number;
    bwl: number;
    hydration:number;
    inverse: number;
    newUreaqaScore: number;
    newInverse: number;
    absRate:number;
    hyd_factor: number;
    time_amount: number;
    drinkAmount: number;
    wait: number;
    uid: string;
    unit: string;
    hyd_desc: string;
    chartColor: string;
    hyd_msg: string;
    isOldTest:boolean;
    hasInsights:boolean;

}