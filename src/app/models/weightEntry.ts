export interface WeightEntry {
    weight_kg:number;
    timestamp:String;
}