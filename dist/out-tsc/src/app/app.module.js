var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationSetupModalPageModule } from './notification-setup-modal/notification-setup-modal.module';
import { NotificationSetupModalPage } from './notification-setup-modal/notification-setup-modal.page';
import { HttpClientModule } from '@angular/common/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { ChartsModule } from 'ng2-charts';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FIREBASE_CONFIG } from './app.firebase.config';
import { FirestoreService } from './services/firestore.service';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [AppComponent],
            entryComponents: [NotificationSetupModalPage],
            imports: [
                BrowserModule, BrowserAnimationsModule, HttpClientModule,
                IonicModule.forRoot(),
                AppRoutingModule,
                ChartsModule,
                AngularFireModule.initializeApp(FIREBASE_CONFIG),
                AngularFireAuthModule,
                AngularFirestoreModule,
                NotificationSetupModalPageModule
            ],
            providers: [
                StatusBar,
                SplashScreen,
                LocalNotifications,
                AngularFirestore,
                FirebaseAnalytics,
                AppVersion,
                FirestoreService,
                { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
/*
IonicModule.forRoot(MyApp, {
  menuType: 'push',
  platforms: {
    ios: {
      menuType: 'overlay',
    }
  }
}),*/ 
//# sourceMappingURL=app.module.js.map