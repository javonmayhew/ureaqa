var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Injectable } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import * as moment from 'moment';
var NotifyService = /** @class */ (function () {
    function NotifyService(platform, alertCtrl, localNotifications) {
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.localNotifications = localNotifications;
        this.notifications = [];
        this.notifyTime = moment(new Date()).format();
        this.chosenHours = new Date().getHours();
        this.chosenMinutes = new Date().getMinutes();
    }
    NotifyService.prototype.checkNotifications = function () {
        return __awaiter(this, void 0, void 0, function () {
            var scheduledNotifications, msg, alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.localNotifications.getAllScheduled()];
                    case 1:
                        scheduledNotifications = _a.sent();
                        msg = scheduledNotifications.toString();
                        return [4 /*yield*/, this.alertCtrl.create({
                                header: 'Scheduled Notifications: ',
                                message: msg,
                                buttons: ['Ok']
                            })];
                    case 2:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    NotifyService.prototype.timeChange = function (time) {
        this.chosenHours = time.hour.value;
        this.chosenMinutes = time.minute.value;
    };
    NotifyService.prototype.oneReminder = function (drinkAmount, unit) {
        var msg = "Did you drink " + drinkAmount + " " + unit + " over the last hour?";
        var reminders = [{
                id: 11,
                text: msg,
                trigger: { at: new Date(new Date().getTime() + (3600 * 1000 * 1)) }
            }];
        this.localNotifications.schedule(reminders);
        console.log("Notifications to be scheduled: ", reminders);
    };
    NotifyService.prototype.twoReminders = function (drinkAmount, unit, time_amount) {
        var milestone = drinkAmount / 2;
        var msg = "Halfway there! Did you drink " + milestone + " " + unit + " yet?";
        var msg2 = "Did you drink " + drinkAmount + " " + unit + " over the last " + time_amount + " hours?";
        var reminders = [{
                id: 11,
                text: msg,
                trigger: { at: new Date(new Date().getTime() + (3600 * 1000 * time_amount / 2)) }
            }, {
                id: 12,
                text: msg2,
                trigger: { at: new Date(new Date().getTime() + (3600 * 1000 * time_amount)) }
            }];
        this.localNotifications.schedule(reminders);
        console.log("Notifications to be scheduled: ", reminders);
    };
    NotifyService.prototype.testReminder = function (waitTime) {
        //wait time in hours
        var wait = new Date(new Date().getTime() + (3600 * 1000 * waitTime));
        this.localNotifications.schedule({
            id: 9,
            text: 'Time to test again!',
            trigger: { at: wait }
        });
    };
    NotifyService.prototype.cancelAll = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.localNotifications.cancelAll();
                        return [4 /*yield*/, this.alertCtrl.create({
                                message: 'Notifications cancelled',
                                buttons: ['Ok']
                            })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    NotifyService.prototype.testNotification = function (wait) {
        var msg = "Here's a message";
        this.localNotifications.schedule({
            text: 'This is a Local Notification',
            trigger: { at: new Date(new Date().getTime() + wait) },
            led: 'FF0000'
        });
    };
    NotifyService.prototype.addNotifications = function (days, time) {
        return __awaiter(this, void 0, void 0, function () {
            var currentDate, currentDay, _i, _a, day, firstNotificationTime, dayDifference, notification, ID, cancelID, _b, ID_1, id;
            var _this = this;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        //this.days
                        //firstNotificationTime
                        this.notifications = [];
                        this.days = days;
                        this.chosenHours = time.getHours();
                        this.chosenMinutes = time.getMinutes();
                        currentDate = new Date();
                        currentDay = currentDate.getDay();
                        for (_i = 0, _a = this.days; _i < _a.length; _i++) {
                            day = _a[_i];
                            if (day.checked) {
                                firstNotificationTime = new Date();
                                dayDifference = day.dayCode - currentDay;
                                if (dayDifference < 0) {
                                    dayDifference = dayDifference + 7; // for cases where the day is in the following week
                                }
                                firstNotificationTime.setHours(firstNotificationTime.getHours() + (24 * (dayDifference)));
                                firstNotificationTime.setHours(this.chosenHours);
                                firstNotificationTime.setMinutes(this.chosenMinutes);
                                notification = {
                                    id: day.dayCode,
                                    title: 'Test Reminder',
                                    text: 'Remember to check your hydration today!',
                                    trigger: { at: firstNotificationTime, every: 'week' }
                                };
                                this.notifications.push(notification);
                            }
                        }
                        console.log("Notifications to be scheduled: ", this.notifications);
                        if (!this.platform.is('cordova')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.localNotifications.getScheduledIds()];
                    case 1:
                        ID = _c.sent();
                        cancelID = [];
                        for (_b = 0, ID_1 = ID; _b < ID_1.length; _b++) {
                            id = ID_1[_b];
                            if (id < 6) {
                                cancelID.push(id);
                            }
                        }
                        console.log("Notifications to be rescheduled");
                        console.log(cancelID);
                        //var ID = [0,1,2,3,4,5,6];
                        this.localNotifications.cancel(cancelID).then(function () { return __awaiter(_this, void 0, void 0, function () {
                            var msg, alert;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        // Schedule the new notifications
                                        this.localNotifications.schedule(this.notifications);
                                        msg = this.notifications.toString;
                                        this.notifications.length = 0;
                                        return [4 /*yield*/, this.alertCtrl.create({
                                                message: ('Notifications set: ' + msg),
                                                buttons: ['Ok']
                                            })];
                                    case 1:
                                        alert = _a.sent();
                                        return [4 /*yield*/, alert.present()];
                                    case 2:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        _c.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    NotifyService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [Platform, AlertController, LocalNotifications])
    ], NotifyService);
    return NotifyService;
}());
export { NotifyService };
//# sourceMappingURL=notify.service.js.map