var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
var FirestoreService = /** @class */ (function () {
    function FirestoreService(http, db) {
        this.http = http;
        this.db = db;
    }
    FirestoreService.prototype.createProfile = function (username, firstName, lastName, email, id) {
        //const id = this.firestore.createId(); //might want to not do this
        return this.db.doc("users/" + id).set({
            id: id,
            username: username,
            firstName: firstName,
            lastName: lastName,
            email: email,
        });
    };
    FirestoreService.prototype.getProfileList = function () {
        return this.db.collection("users");
    };
    FirestoreService.prototype.getProfile = function (id) {
        return this.db.doc("users/" + id);
    };
    FirestoreService.prototype.getCurrentWeight = function (id) {
        return this.db.doc("users/" + id + "/weight/" + id);
    };
    FirestoreService.prototype.setCurrentWeight = function (id, weight_kg) {
        //const docID = this.db.createId(); //might want to not do this
        var date = new Date();
        var timestamp = date.toISOString();
        return this.db.doc("users/" + id + "/weight/" + id).set({
            weight_kg: weight_kg,
            timestamp: timestamp
        });
    };
    FirestoreService.prototype.updateCurrentWeight = function (id, weight_kg) {
        //const docID = this.db.createId(); //might want to not do this
        var date = new Date();
        var timestamp = date.toISOString();
        return this.db.doc("users/" + id + "/weight/" + id).update({
            weight_kg: weight_kg,
            timestamp: timestamp
        });
    };
    FirestoreService.prototype.getHydration = function (sg) {
        var m = 213.87;
        var b = 214.87;
        sg = 1 + sg / 1000;
        var bwPercent = sg * m - b;
        var hydration = Math.round(100 - 5 * bwPercent);
        return hydration;
    };
    FirestoreService.prototype.addTestResults = function (id, sg, time, weight_kg, perceived_hyd) {
        var m = 213.87;
        var b = 214.87;
        sg = 1 + sg / 1000;
        var bwPercent = sg * m - b;
        var hydration = Math.round(100 - 5 * bwPercent);
        var bwl = weight_kg * bwPercent / 100;
        var hyd_factor = bwl * Math.abs(100 - hydration); // 0-180, this is the "hydration insights" number
        var hyd_score = Math.round((200 - hyd_factor)) / 10; //0-20, this is the "new" ureaqa score
        var docID = this.db.createId(); //might want to not do this
        var uid = id;
        return this.db.doc("results/" + docID).set({
            bwl: bwl,
            hydration: hydration,
            perceived_hyd: perceived_hyd,
            sg: sg,
            time: time,
            uid: uid,
        });
    };
    FirestoreService.prototype.getTestResults = function (uid) {
        //find this users 3 most recent results
        return this.db.collection("results", function (ref) { return ref.where("uid", "==", uid).orderBy("time", "desc").limit(3); });
    };
    FirestoreService.prototype.getSomeTestResults = function (uid) {
        //find this users 5 most recent results
        return this.db.collection("results", function (ref) { return ref.where("uid", "==", uid).orderBy("time", "desc").limit(5); });
    };
    FirestoreService.prototype.getWeeklyResults = function (uid, cutoff) {
        //find this users 10 most recent results
        var result = this.db.collection("results", function (ref) { return ref.where("uid", "==", uid).where("time", ">=", cutoff).orderBy("time").limit(10); });
        return result;
    };
    FirestoreService.prototype.getOneTest = function (docID) {
        return this.db.doc("results/" + docID);
    };
    FirestoreService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient, AngularFirestore])
    ], FirestoreService);
    return FirestoreService;
}());
export { FirestoreService };
//# sourceMappingURL=firestore.service.js.map