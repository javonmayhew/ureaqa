var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { AlertController, NavController, ToastController, Platform } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { ModalController } from '@ionic/angular';
import { NotificationSetupModalPage } from '../../notification-setup-modal/notification-setup-modal.page';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(formBuilder, afAuth, firebaseAnalytics, localNotifications, toast, router, afProvider, alertCtrl, modalController, navCtrl, platform) {
        this.afAuth = afAuth;
        this.firebaseAnalytics = firebaseAnalytics;
        this.localNotifications = localNotifications;
        this.toast = toast;
        this.router = router;
        this.afProvider = afProvider;
        this.alertCtrl = alertCtrl;
        this.modalController = modalController;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.avatarExists = false;
        this.showForm = false;
        this.kg_lbs = 2.20462;
        this.imageURL = "";
        /*
        notifyTime: any;
        notifications: any[] = [];
        days: any[];
        chosenHours: number;
        chosenMinutes: number;
      
      */
        this.backgroundColor = "#555";
        this.sub = new Subscription();
        this.subAuth = new Subscription();
        this.updateWeightForm = formBuilder.group({
            weight: ['', Validators.compose([Validators.required, Validators.min(50), Validators.max(500)])]
        });
    }
    ProfilePage.prototype.ionViewWillEnter = function () {
        console.log('ionViewDidLoad ProfilePage');
        this.firebaseAnalytics.setCurrentScreen('ProfilePage');
    };
    ProfilePage.prototype.ionViewWillLeave = function () {
        console.log("Profile Page Will Leave");
        if (!this.sub.closed) {
            console.log("Unsubscribing from test results...");
            this.sub.unsubscribe();
        }
        if (!this.subAuth.closed) {
            console.log("Unsubscribing from auth details...");
            this.subAuth.unsubscribe();
        }
    };
    ProfilePage.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Initializing New Test Page");
        this.subAuth = this.afAuth.authState.subscribe(function (user) {
            console.log("Subscribing to auth details...");
            if (user && user.uid) {
                _this.userId = user.uid;
                if (!_this.subAuth.closed) {
                    console.log("Unsubscribing from auth details...");
                    _this.subAuth.unsubscribe();
                }
                else {
                    console.log("Already unsubscribed from auth details!");
                }
                _this.profile = _this.afProvider.getProfile(_this.userId).valueChanges();
                //const imgRef: AngularFirestoreDocument<Image> = this.afProvider.getProfilePic(this.userId);
                //imgRef.valueChanges().subscribe(res=>{if(res){this.imageURL = res.imageURL;}else{this.imageURL="";}}); 
                //if(this.imageURL.length > 1){this.avatarExists = true;}
                _this.weightEntry = _this.afProvider.getCurrentWeight(_this.userId).valueChanges();
                _this.sub = _this.weightEntry.subscribe(function (res) {
                    console.log("Subscribing to weight results...");
                    _this.weight_kg = res.weight_kg;
                    _this.currentWeight = Math.round(_this.weight_kg * _this.kg_lbs);
                    if (!_this.sub.closed) {
                        console.log("Unsubscribing from weight results...");
                        _this.sub.unsubscribe();
                    }
                    if (!_this.subAuth.closed) {
                        console.log("Unsubscribing from auth details...");
                        _this.subAuth.unsubscribe();
                    }
                    else {
                        console.log("Already unsubscribed from auth details!");
                    }
                });
            }
            else {
                console.log("Could not find authentication details");
                _this.router.navigateByUrl("/login");
            }
        });
    };
    ProfilePage.prototype.ngOnDestroy = function () {
        console.log("Destroying Home Page");
    };
    ProfilePage.prototype.toggleForm = function () {
        this.showForm = !this.showForm;
    };
    ProfilePage.prototype.presentToast = function (msg) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toast.create({
                            message: msg,
                            duration: 1000,
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfilePage.prototype.presentModal = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: NotificationSetupModalPage
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ProfilePage.prototype.updateWeight = function () {
        this.currentWeight = this.updateWeightForm.value.weight;
        this.weight_kg = this.currentWeight / this.kg_lbs;
        this.afProvider.updateCurrentWeight(this.userId, this.weight_kg);
        this.presentToast("Weight Updated");
        this.showForm = !this.showForm;
    };
    ProfilePage = __decorate([
        Component({
            selector: 'app-profile',
            templateUrl: './profile.page.html',
            styleUrls: ['./profile.page.scss'],
        }),
        __metadata("design:paramtypes", [FormBuilder, AngularFireAuth, FirebaseAnalytics,
            LocalNotifications, ToastController, Router,
            FirestoreService, AlertController, ModalController,
            NavController, Platform])
    ], ProfilePage);
    return ProfilePage;
}());
export { ProfilePage };
//# sourceMappingURL=profile.page.js.map