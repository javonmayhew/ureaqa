var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component, NgZone } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { NavController, ToastController, Platform } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { NotifyService } from '../../services/notify.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
var HomePage = /** @class */ (function () {
    function HomePage(afProvider, notify, afAuth, firebaseAnalytics, navCtrl, router, zone, toast, platform, appVersion) {
        var _this = this;
        this.afProvider = afProvider;
        this.notify = notify;
        this.afAuth = afAuth;
        this.firebaseAnalytics = firebaseAnalytics;
        this.navCtrl = navCtrl;
        this.router = router;
        this.zone = zone;
        this.toast = toast;
        this.platform = platform;
        this.appVersion = appVersion;
        this.ureaqaScore = 0;
        this.inverseScore = 100 - this.ureaqaScore;
        this.showResults = true;
        this.unknownMsg = null;
        this.graph = false;
        // Doughnut
        this.doughnutChartLabels = [];
        this.doughnutChartData = [];
        this.doughnutChartType = 'doughnut';
        //backgroundColor: ["#00afa0", "#ffffff"],
        //hoverBackgroundColor: ["#00afa0", "#ffffff"],
        //hoverBorderColor: ["#fff0","#fff0" ],
        //hoverBorderWidth: [0,0],
        //borderWidth: [0,0]
        this.doughnutChartOptions = {
            legend: {
                display: false
            },
            tooltips: {
                displayColors: false,
                callbacks: {
                    label: (function (tooltipItem) {
                        var index = tooltipItem.index;
                        var label = _this.doughnutChartLabels[index];
                        return label;
                    })
                }
            }
        };
        this.sub = new Subscription();
        this.subAuth = new Subscription();
    }
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        //this.sub.subscribe();
        //this.subAuth.subscribe();
        console.log("Home Page Will Enter");
        this.firebaseAnalytics.setCurrentScreen('HomePage');
        this.mobilePlatform = this.platform.platforms();
        this.appVersion.getVersionNumber()
            .then(function (res) { return _this.versionNumber = res; })
            .catch(function (error) { return console.error(error); });
    };
    HomePage.prototype.ionViewWillLeave = function () {
        console.log("Home Page Will Leave");
        if (!this.sub.closed) {
            console.log("Unsubscribing from test results...");
            this.sub.unsubscribe();
        }
        if (!this.subAuth.closed) {
            console.log("Unsubscribing from auth details...");
            this.subAuth.unsubscribe();
        }
    };
    HomePage.prototype.ngOnDestroy = function () {
        console.log("Destroying Home Page");
    };
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Initializing Home Page");
        this.afAuth.auth.onAuthStateChanged(function (user) {
            if (!user) {
                console.log("logging out...");
                // No user is signed in
                _this.router.navigateByUrl('/login');
            }
            else {
                console.log("Logged in!");
            }
        });
        this.subAuth = this.afAuth.authState.subscribe(function (user) {
            console.log("Subscribing to auth details...");
            if (user && user.uid) {
                _this.uid = user.uid;
                if (!_this.subAuth.closed) {
                    console.log("Unsubscribing from auth details...");
                    _this.subAuth.unsubscribe();
                }
                else {
                    console.log("Already unsubscribed from auth details!");
                }
                _this.firebaseAnalytics.setUserId(_this.uid);
                var testTime = new Date(0);
                var self = _this;
                _this.testList = _this.afProvider.getTestResults(_this.uid).valueChanges();
                _this.sub = _this.testList.subscribe(function (res) {
                    console.log("Subscribing to test results...");
                    console.log(res);
                    if (res && res.length > 1) {
                        _this.showResults = true;
                        res.forEach(function (doc) {
                            console.log("New Test:");
                            console.log(doc.sg);
                            var data = new Date(doc.time);
                            var dateString = data.toISOString().split('T').shift();
                            console.log(dateString);
                            if (data.valueOf() > testTime.valueOf()) {
                                testTime = data;
                                self.lastTestTime = testTime;
                                self.ureaqaScore = doc.hydration;
                                self.inverseScore = 100 - self.ureaqaScore;
                                self.bwl = doc.bwl;
                                self.lastTest = doc;
                                self.waterIntake = self.hydrationInsights(self.bwl, self.ureaqaScore);
                            }
                        });
                        console.log("Tests should be done");
                    }
                    else {
                        //No results yet
                        _this.showResults = false;
                        _this.doughnutChartData = [100, 0];
                        _this.unknownMsg = "N/A";
                        _this.ureaqaScore = null;
                        _this.doughnutChartData = [0, 100];
                        _this.hyd_msg = "Try adding a new test to see your personalized hydration plan.";
                    }
                    if (!_this.sub.closed) {
                        console.log("Unsubscribing from test results...");
                        _this.sub.unsubscribe();
                    }
                    if (!_this.subAuth.closed) {
                        console.log("Unsubscribing from auth details...");
                        _this.subAuth.unsubscribe();
                    }
                    else {
                        console.log("Already unsubscribed from auth details!");
                    }
                }, function (error) {
                    console.error(error);
                });
                //endif
                console.log("end if");
            }
            else {
                console.log("Could not find authentication details");
            }
        });
    };
    HomePage.prototype.newTest = function () {
        var _this = this;
        console.log('Button Clicked');
        this.zone.run(function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.router.navigateByUrl('/new-test')];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
        /*
        this.firebaseAnalytics.logEvent('new_test_nav_clicked',{uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
        .then((res: any) => console.log(res))
        .catch((error: any) => console.error(error));
        */
    };
    HomePage.prototype.hydrationInsights = function (waterDeficit, ureaqaScore) {
        var _this = this;
        //hourly water absorbsion rate: 500 mL or 1 L in extreme heat
        this.graph = false;
        var inverse = 100 - ureaqaScore;
        var absRate = 0.5;
        var hyd_factor = waterDeficit * inverse;
        var time_amount;
        var drinkAmount;
        var reminders;
        if (waterDeficit < 1) {
            //use mL
            waterDeficit = waterDeficit * 1000;
            absRate = 500;
            this.unit = "mL";
        }
        else {
            absRate = 0.5;
            this.unit = "L";
        }
        var wait;
        if (hyd_factor <= 5) {
            //fully hydrated
            this.hyd_desc = "Fully Hydrated";
            this.chartColor = '#19B9A5';
            //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
            drinkAmount = waterDeficit; //100 to 990 mL
            if (drinkAmount > 10) {
                drinkAmount = Math.round(drinkAmount / 50) * 50; //round to nearest 50 mL
            }
            time_amount = Math.round(drinkAmount / absRate) + 1;
            if (time_amount < 2) {
                this.hyd_msg = "Drink over the next hour. Test again as needed.";
                this.notify.oneReminder(drinkAmount, this.unit);
            }
            else {
                this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again as needed.";
                this.notify.twoReminders(drinkAmount, this.unit, time_amount);
            }
        }
        else if (hyd_factor <= 20) {
            //mild dehydration
            this.hyd_desc = "Hydrated";
            this.chartColor = '#5AC89B';
            //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
            drinkAmount = waterDeficit; //1 to 2 L
            time_amount = Math.round(drinkAmount / absRate) + 1;
            if (drinkAmount < 3) {
                drinkAmount = Math.round(drinkAmount * 10) / 10; //round to nearest 100 mL
            }
            else {
                drinkAmount = Math.round(drinkAmount / 50) * 50; //round to nearest 50 mL
            }
            if (time_amount < 2) {
                this.hyd_msg = "Drink over the next hour. Test again as needed.";
                this.notify.oneReminder(drinkAmount, this.unit);
            }
            else {
                this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again as needed.";
                this.notify.twoReminders(drinkAmount, this.unit, time_amount);
            }
        }
        else if (hyd_factor <= 36) {
            //moderate dehydration
            //1.4 to 2.5 L deficit
            this.hyd_desc = "Mildly Dehydrated";
            this.chartColor = '#E6E688';
            //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
            drinkAmount = waterDeficit * 0.8; // 1 to 2 L
            time_amount = Math.round(drinkAmount / absRate) + 1; //3 to 5 hours
            if (drinkAmount < 5) {
                drinkAmount = Math.round(drinkAmount * 10) / 10; //round to nearest 100 mL
            }
            else {
                drinkAmount = Math.round(drinkAmount / 50) * 50; //round to nearest 50 mL
            }
            this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again after your next workout.";
            this.notify.twoReminders(drinkAmount, this.unit, time_amount);
        }
        else if (hyd_factor <= 56) {
            //clinical dehydration
            //1.8 to 3.4 L deficit
            this.hyd_desc = "Moderately Dehydrated";
            this.chartColor = '#FFEB84';
            //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
            drinkAmount = waterDeficit * 0.8; // 1.4 to 2.7 L
            time_amount = Math.round(drinkAmount / absRate) + 1; //3 to 6 hours
            drinkAmount = Math.round(drinkAmount / 100) * 100; //round to nearest 100 mL
            this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again at least 2 hours before your next workout.";
            this.notify.twoReminders(drinkAmount, this.unit, time_amount);
        }
        else if (hyd_factor <= 80) {
            //clinical dehydration
            this.hyd_desc = "Dehydrated";
            //2.3 - 4 L deficit
            this.chartColor = '#FFC87D';
            //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
            drinkAmount = waterDeficit * 0.65; // 1.5 to 2.6 L
            time_amount = Math.round(drinkAmount / absRate); //3 to 5 hours
            if (drinkAmount < 5) {
                drinkAmount = Math.round(drinkAmount * 10) / 10; //round to nearest 100 mL
            }
            else {
                drinkAmount = Math.round(drinkAmount / 100) * 100; //round to nearest 100 mL
            }
            wait = time_amount * 1.5;
            this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again after " + wait + " hours.";
            this.notify.twoReminders(drinkAmount, this.unit, time_amount);
            this.notify.testReminder(wait);
        }
        else if (hyd_factor <= 108) {
            //clinical-severe dehydration
            this.hyd_desc = "Dehydrated";
            //2.7 - 4.2 L deficit
            this.chartColor = '#FFA578';
            //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
            drinkAmount = waterDeficit * 0.65; // 1.7 to 2.7 L
            time_amount = Math.round(drinkAmount / absRate) - 1; //2 to 4 hours
            if (drinkAmount < 5) {
                drinkAmount = Math.round(drinkAmount * 10) / 10; //round to nearest 100 mL
            }
            else {
                drinkAmount = Math.round(drinkAmount / 100) * 100; //round to nearest 100 mL
            }
            wait = time_amount * 1.5;
            this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again after " + wait + " hours.";
            this.notify.twoReminders(drinkAmount, this.unit, time_amount);
            this.notify.testReminder(wait);
        }
        else if (hyd_factor <= 126) {
            //severe dehydration
            this.hyd_desc = "Very Dehydrated";
            //3.8 - 5 L deficit
            this.chartColor = '#FA8771';
            //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
            drinkAmount = waterDeficit * 0.5; // 1.9 to 2.5 L
            time_amount = Math.round(drinkAmount / absRate) - 2; //2 to 3 hours
            if (drinkAmount < 5) {
                drinkAmount = Math.round(drinkAmount * 10) / 10; //round to nearest 100 mL
            }
            wait = time_amount + 1;
            this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again as soon as possible.";
            this.notify.twoReminders(drinkAmount, this.unit, time_amount);
            this.notify.testReminder(2.5);
        }
        else if (hyd_factor <= 180) {
            //severe dehydration
            this.hyd_desc = "Very Dehydrated";
            //4.5 - 6 L deficit
            this.chartColor = '#FA7070';
            //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
            drinkAmount = waterDeficit * 0.5; // 2.2 to 3 L
            time_amount = Math.round(drinkAmount / absRate) - 2; //2 to 4 hours
            if (drinkAmount < 5) {
                drinkAmount = Math.round(drinkAmount * 10) / 10; //round to nearest 100 mL
            }
            this.hyd_msg = "Drink over the next " + time_amount + " hours. Test again as soon as possible.";
            this.notify.twoReminders(drinkAmount, this.unit, time_amount);
            this.notify.testReminder(2.5);
        }
        else {
            //SEVERE dehydration
            this.hyd_desc = "Severely Dehydrated";
            this.chartColor = '#FA5050';
            //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
            drinkAmount = waterDeficit;
            time_amount = Math.round(drinkAmount / absRate) - 2;
            if (drinkAmount < 10) {
                drinkAmount = Math.round(drinkAmount * 10) / 10; //round to nearest 100 mL
            }
            this.hyd_msg = "Are you feeling okay? Monitor your hydration closely over the next few hours.";
            this.notify.twoReminders(drinkAmount, this.unit, time_amount);
            this.notify.testReminder(2.5);
        }
        this.doughnutChartData.length = 0;
        this.doughnutChartLabels.length = 0;
        this.doughnutChartColors = [{
                backgroundColor: [this.chartColor, "#ffffff"],
                hoverBackgroundColor: [this.chartColor, "#ffffff"],
                hoverBorderColor: ["#fff0", "#fff0"],
                hoverBorderWidth: [0, 0],
                borderWidth: [0, 0]
            }];
        console.log(this.doughnutChartColors);
        this.doughnutChartData = [this.ureaqaScore, this.inverseScore];
        this.doughnutChartLabels = ['Hydration', 'Dehydration'];
        setTimeout(function () { _this.graph = true; }, 0);
        /*
        this.toast.create({
          message: this.chartColor,
          duration: 3000,
          position: 'bottom'
        }).present();
        */
        //this.doughnutChartColors = [{ backgroundColor: [this.chartColor, "#0000"] }];
        return drinkAmount;
    };
    HomePage = __decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        __metadata("design:paramtypes", [FirestoreService, NotifyService, AngularFireAuth, FirebaseAnalytics,
            NavController, Router, NgZone, ToastController, Platform, AppVersion])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map