var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { FormBuilder, Validators } from '@angular/forms';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Router } from '@angular/router';
var CreateProfilePage = /** @class */ (function () {
    function CreateProfilePage(navCtrl, formBuilder, toastCtrl, firestoreProvider, afAuth, firebaseAnalytics, loadingCtrl, router) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.firestoreProvider = firestoreProvider;
        this.afAuth = afAuth;
        this.firebaseAnalytics = firebaseAnalytics;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.sub = this.afAuth.authState.subscribe(function (user) {
            if (user && user.uid) {
                _this.userId = user.uid;
                _this.email = user.email;
            }
        });
        this.createProfileForm = formBuilder.group({
            username: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
            firstName: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
            lastName: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
            weight: ['', Validators.compose([Validators.required, Validators.min(75), Validators.max(300)])]
        });
    }
    //put it inside of a provider?
    CreateProfilePage.prototype.createProfile = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loading, username, firstName, lastName, email, uid, weight_kg, user;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        username = this.createProfileForm.value.username;
                        firstName = this.createProfileForm.value.firstName;
                        lastName = this.createProfileForm.value.lastName;
                        email = this.email;
                        uid = this.userId;
                        weight_kg = this.createProfileForm.value.weight / 2.20462;
                        this.firestoreProvider.setCurrentWeight(uid, weight_kg);
                        user = this.afAuth.auth.currentUser;
                        user.updateProfile({ displayName: firstName, photoURL: null })
                            .then(function () {
                        }, function (error) {
                            console.log(error);
                            _this.presentToast("Something went wrong. Couldn't update profile.");
                        });
                        this.firestoreProvider.createProfile(username, firstName, lastName, email, uid).then(function () {
                            loading.dismiss().then(function () {
                                _this.router.navigateByUrl('/profile');
                            });
                        }, function (error) {
                            console.error(error);
                            loading.dismiss();
                            _this.presentToast("Verify your email to continue.");
                        });
                        return [4 /*yield*/, loading.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    CreateProfilePage.prototype.presentToast = function (msg) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 3000,
                            position: 'middle'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    CreateProfilePage.prototype.ngOnDestroy = function () {
        //unsubscribe!!
        if (this.sub) {
            this.sub.unsubscribe();
        }
    };
    CreateProfilePage.prototype.ngOnInit = function () {
        console.log('ionViewDidLoad CreateProfilePage');
        this.firebaseAnalytics.setCurrentScreen('CreateProfilePage');
    };
    CreateProfilePage = __decorate([
        Component({
            selector: 'app-create-profile',
            templateUrl: './create-profile.page.html',
            styleUrls: ['./create-profile.page.scss'],
        }),
        __metadata("design:paramtypes", [NavController, FormBuilder, ToastController,
            FirestoreService, AngularFireAuth, FirebaseAnalytics,
            LoadingController, Router])
    ], CreateProfilePage);
    return CreateProfilePage;
}());
export { CreateProfilePage };
//# sourceMappingURL=create-profile.page.js.map