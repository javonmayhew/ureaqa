var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
var InsightsPage = /** @class */ (function () {
    function InsightsPage(afProvider, afAuth, firebaseAnalytics, toast, navCtrl, router) {
        this.afProvider = afProvider;
        this.afAuth = afAuth;
        this.firebaseAnalytics = firebaseAnalytics;
        this.toast = toast;
        this.navCtrl = navCtrl;
        this.router = router;
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };
        this.barChartLabels = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
        this.barChartLabelsRecent = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
        this.barChartType = 'bar';
        this.barChartLegend = true;
        this.barChartColors = [{ backgroundColor: "#00afa0", borderColor: "#0000" }, { backgroundColor: "#606060", borderColor: "#0000" }];
        this.hydTestData = [];
        this.pHydTestData = [];
        this.hydData = [0, 0, 0, 0, 0, 0, 0];
        this.pHydData = [0, 0, 0, 0, 0, 0, 0];
        this.optHydData = [0, 0, 0, 0, 0, 0, 0];
        this.testDay = [];
        this.decision = "lastFive";
        this.weeklyData = false;
        this.showData = false;
        this.barChartDataSet = [
            { data: [], label: 'Hydration Score' },
            { data: [0, 0, 0, 0, 0, 0, 0], label: 'Perceived Hydration' }
        ];
        this.barChartData = [{ data: [] }];
        this.sub = new Subscription();
        this.subAuth = new Subscription();
        console.log("Constructing Insights Page");
    }
    InsightsPage.prototype.ngOnInit = function () {
        var _this = this;
        console.log('Initializing Insights Page');
        this.subAuth = this.afAuth.authState
            .subscribe(function (user) {
            console.log("Subscribing to auth details...");
            if (user && user.uid) {
                _this.uid = user.uid;
                _this.displayOptions = "lastFive";
                if (_this.uid) {
                    _this.getRecentTests();
                }
                if (!_this.subAuth.closed) {
                    console.log("Unsubscribing from auth details...");
                    _this.subAuth.unsubscribe();
                }
                else {
                    console.log("Already unsubscribed from auth details!");
                }
            }
            else {
                console.log("Could not find authentication details");
                _this.router.navigateByUrl("/login");
            }
        }, function (error) {
            console.log('Authentication Error');
            console.log(error);
        });
    };
    InsightsPage.prototype.ngOnDestroy = function () {
        console.log("Destroying Insights Page ");
        this.sub.unsubscribe();
        this.subAuth.unsubscribe();
    };
    InsightsPage.prototype.ionViewWillEnter = function () {
        console.log('Insights Page Will Enter');
        this.firebaseAnalytics.setCurrentScreen('InsightsPage');
    };
    InsightsPage.prototype.ionViewWillLeave = function () {
        console.log("Insights Page Will Leave");
        if (!this.sub.closed) {
            console.log("Unsubscribing from test results...");
            this.sub.unsubscribe();
        }
        if (!this.subAuth.closed) {
            console.log("Unsubscribing from auth details...");
            this.subAuth.unsubscribe();
        }
    };
    InsightsPage.prototype.makeDecision = function () {
        var newDecision = this.displayOptions;
        if (this.decision !== newDecision) {
            //do something
            this.decision = newDecision;
            if (newDecision == "thisWeek") {
                this.weeklyData = true;
                this.getThisWeek();
            }
            else if (newDecision == "lastWeek") {
                this.weeklyData = true;
                this.getLastWeek();
            }
            else if (newDecision == "lastFive") {
                this.weeklyData = false;
                this.getRecentTests();
            }
        }
        else if (newDecision == "lastFive") {
            this.weeklyData = false;
            this.getRecentTests();
        }
    };
    InsightsPage.prototype.doTheMath = function () {
        console.log("Starting Analysis");
        var hydMin = function (arr) { return Math.min.apply(Math, arr); };
        var hydMax = function (arr) { return Math.max.apply(Math, arr); };
        var hydAvg = (function (arr) {
            if (arr) {
                var val = arr.reduce(function (a, b) { return a + b; }, 0);
                return val;
            }
            else {
                return 0;
            }
        });
        this.hydMin = hydMin(this.hydTestData);
        this.hydMax = hydMax(this.hydTestData);
        this.hydAvg = Math.round(hydAvg(this.hydTestData) / this.testCount);
        this.pHydAvg = Math.round(hydAvg(this.pHydTestData) / this.testCount);
        console.log(this.hydAvg);
        console.log(this.pHydAvg);
        this.hydTestData.push(0);
        this.pHydTestData.push(0);
        //this.barChartDataSet = [{data: this.hydData, label: 'Hydration Score'},{data: this.pHydData, label: 'Perceived Hydration'}];
        if (this.hydAvg == 0) {
            this.hydMin = 0;
            this.hydMax = 0;
            this.presentToast("Could not find this week's test results");
        }
        else { //things are normal
            //this.sgMin = this.sgMin/1000 + 1;
            //this.sgMax = this.sgMax/1000 + 1;
            //this.sgAvg = this.sgAvg/1000 + 1;
        }
        console.log(this.hydData);
        console.log('Test Count: ' + this.testCount);
        console.log('Perceived Hydration: ' + this.pHydAvg);
        console.log('Average Hydration: ' + this.hydAvg);
    };
    InsightsPage.prototype.getRecentTests = function () {
        var _this = this;
        console.log("get recent tests");
        this.showData = false;
        //this.barChartData.length = 0;
        this.barChartLabels.length = 0;
        this.hydData.length = 0;
        this.pHydData.length = 0;
        var self = this;
        var labels = [];
        console.log(this.uid);
        this.testList = this.afProvider.getSomeTestResults(this.uid).valueChanges();
        this.sub = this.testList.subscribe(function (res) {
            console.log("Subscribing to test results...");
            console.log(res.length + " Results Found");
            if (res) {
                self.testCount = res.length;
                var i_1 = 0;
                var j_1 = res.length - 1; //biggest array number is 4
                labels.length = 0;
                self.hydTestData.length = 0;
                self.pHydTestData.length = 0;
                labels.length = j_1;
                self.hydTestData.length = j_1;
                self.pHydTestData.length = j_1;
                res.forEach(function (doc) {
                    var k = j_1 - i_1; //4, 3, 2, 1, 0
                    var data = Math.round(doc.hydration);
                    self.hydTestData[k] = data;
                    var data2 = doc.perceived_hyd;
                    self.pHydTestData[k] = data2;
                    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var date = new Date(doc.time);
                    var dateString = date.toISOString().split('T').shift();
                    var d = new Date(dateString);
                    var formatted_date = months[d.getMonth()] + "-" + d.getDate();
                    console.log(formatted_date);
                    labels[k] = formatted_date;
                    i_1 = i_1 + 1;
                });
                _this.hydData = _this.hydTestData;
                _this.pHydData = _this.pHydTestData;
                _this.doTheMath();
                _this.barChartDataSet.length = 0;
                _this.barChartLabels.length = 0;
                //this.barChartLabels.length = 0;
                setTimeout(function () {
                    //updating chart data
                    _this.barChartDataSet = [{ data: _this.hydTestData, label: 'Hydration Score' }, { data: _this.pHydTestData, label: 'Perceived Hydration' }];
                    _this.barChartLabels = labels;
                    _this.showData = true;
                }, 0);
                console.log("Tests should be done");
            }
            if (!_this.sub.closed) {
                console.log("Unsubscribing from test results...");
                _this.sub.unsubscribe();
            }
            if (!_this.subAuth.closed) {
                console.log("Unsubscribing from auth details...");
                _this.subAuth.unsubscribe();
            }
            else {
                console.log("Already unsubscribed from auth details!");
            }
        });
    };
    InsightsPage.prototype.getThisWeek = function () {
        //get monday
        var _this = this;
        this.showData = false;
        //wipe it
        this.barChartDataSet.length = 0;
        this.barChartLabels.length = 0;
        this.hydData.length = 0;
        this.pHydData.length = 0;
        this.hydTestData.length = 0;
        this.pHydTestData.length = 0;
        this.hydData.length = 7;
        this.pHydData.length = 7;
        //initialize it
        this.hydData = [0, 0, 0, 0, 0, 0, 0];
        this.pHydData = [0, 0, 0, 0, 0, 0, 0];
        var labels = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
        var testTime = new Date();
        var d = testTime;
        d = new Date(d);
        var day = d.getDay();
        var diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
        testTime = new Date(d.setDate(diff)); //cutoff date is monday
        var cutoff = testTime.toISOString().split('T').shift();
        console.log(cutoff);
        var self = this;
        this.testList = this.afProvider.getWeeklyResults(this.uid, cutoff).valueChanges(); //Results after Monday
        this.sub = this.testList.subscribe(function (res) {
            console.log("Subscribing to test results...");
            if (res) {
                self.testCount = 0;
                console.log(res.length + " Results Found");
                res.forEach(function (doc, i) {
                    //save each specific gravity
                    var data = Math.round(doc.hydration);
                    var data2 = doc.perceived_hyd;
                    //self.testDay.push(doc.time);
                    var t = new Date(doc.time);
                    var weekcheck = t.valueOf() - testTime.valueOf();
                    if (weekcheck < 605000000) {
                        var day = t.getDay();
                        if (day == 0) {
                            //sunday
                            day = 6;
                        }
                        else {
                            //convert mon-sat
                            day = day - 1;
                        }
                        self.hydTestData.push(data);
                        self.pHydTestData.push(data2);
                        self.testCount = self.testCount + 1;
                        //set Data to the appropriate day
                        console.log(doc.time);
                        console.log(day);
                        if (self.hydData[day] == 0) {
                            self.hydData[day] = data;
                            self.pHydData[day] = data2;
                            self.optHydData[day] = 95;
                            console.log("Adding data to day " + day);
                            console.log(self.hydData);
                        }
                    }
                });
                console.log("Tests should be done");
            }
            else {
                //no results this week
            }
            if (!_this.sub.closed) {
                console.log("Unsubscribing from test results...");
                _this.sub.unsubscribe();
            }
            if (!_this.subAuth.closed) {
                console.log("Unsubscribing from auth details...");
                _this.subAuth.unsubscribe();
            }
            else {
                console.log("Already unsubscribed from auth details!");
            }
            _this.doTheMath();
            _this.barChartDataSet.length = 0;
            _this.barChartLabels.length = 0;
            setTimeout(function () {
                //updating chart data
                _this.barChartDataSet = [{ data: _this.hydData, label: 'Hydration Score' }, { data: _this.pHydData, label: 'Perceived Hydration' }];
                _this.barChartLabels = labels;
                _this.showData = true;
            }, 0);
        });
    };
    InsightsPage.prototype.getLastWeek = function () {
        var _this = this;
        this.showData = false;
        //wipe it
        this.barChartDataSet.length = 0;
        this.barChartLabels.length = 0;
        this.hydData.length = 0;
        this.pHydData.length = 0;
        this.hydTestData.length = 0;
        this.pHydTestData.length = 0;
        //initialize it
        this.hydData = [0, 0, 0, 0, 0, 0, 0];
        this.pHydData = [0, 0, 0, 0, 0, 0, 0];
        var labels = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'];
        //get monday
        var testTime = new Date();
        var d = testTime;
        d = new Date(d);
        var day = d.getDay();
        var diff = d.getDate() - 7 - day + (day == 0 ? -6 : 1); // go back 1 week and adjust when day is sunday
        testTime = new Date(d.setDate(diff)); //cutoff date is monday
        var cutoff = testTime.toISOString().split('T').shift();
        console.log(cutoff);
        var self = this;
        this.testList = this.afProvider.getWeeklyResults(this.uid, cutoff).valueChanges(); //Results after Monday
        this.sub = this.testList.subscribe(function (res) {
            console.log("Subscribing to test results...");
            if (res) {
                self.testCount = 0;
                console.log(res.length + " Results Found");
                res.forEach(function (doc, i) {
                    //save each specific gravity
                    var data = Math.round(doc.hydration);
                    var data2 = doc.perceived_hyd;
                    //self.testDay.push(doc.time);
                    var t = new Date(doc.time);
                    var weekcheck = t.valueOf() - testTime.valueOf();
                    console.log(doc.time);
                    console.log(weekcheck);
                    if (weekcheck < 605000000) {
                        var day = t.getDay();
                        if (day == 0) {
                            //sunday
                            day = 6;
                        }
                        else {
                            //convert mon-sat
                            day = day - 1;
                        }
                        //set Data to the appropriate day
                        self.hydTestData.push(data);
                        self.pHydTestData.push(data2);
                        self.testCount = self.testCount + 1;
                        if (self.hydData[day] == 0) {
                            self.hydData[day] = data;
                            self.pHydData[day] = data2;
                            self.optHydData[day] = 95;
                            console.log("Adding data to day " + day);
                            console.log(self.hydData);
                        }
                    }
                });
                console.log("Tests should be done");
            }
            if (!_this.sub.closed) {
                console.log("Unsubscribing from test results...");
                _this.sub.unsubscribe();
            }
            if (!_this.subAuth.closed) {
                console.log("Unsubscribing from auth details...");
                _this.subAuth.unsubscribe();
            }
            else {
                console.log("Already unsubscribed from auth details!");
            }
            _this.doTheMath();
            _this.barChartDataSet.length = 0;
            _this.barChartLabels.length = 0;
            setTimeout(function () {
                //updating chart data
                _this.barChartDataSet = [{ data: _this.hydData, label: 'Hydration Score' }, { data: _this.pHydData, label: 'Perceived Hydration' }];
                _this.barChartLabels = labels;
                _this.showData = true;
            }, 0);
        });
    };
    InsightsPage.prototype.presentToast = function (msg) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toast.create({
                            message: msg,
                            duration: 3000,
                            position: 'middle'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    InsightsPage = __decorate([
        Component({
            selector: 'app-insights',
            templateUrl: './insights.page.html',
            styleUrls: ['./insights.page.scss'],
        }),
        __metadata("design:paramtypes", [FirestoreService, AngularFireAuth, FirebaseAnalytics,
            ToastController,
            NavController, Router])
    ], InsightsPage);
    return InsightsPage;
}());
export { InsightsPage };
//# sourceMappingURL=insights.page.js.map