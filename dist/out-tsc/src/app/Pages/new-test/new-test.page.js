var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { NavController, LoadingController, ToastController, Platform, AlertController } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
var NewTestPage = /** @class */ (function () {
    function NewTestPage(afProvider, toast, router, afAuth, firestoreProvider, firebaseAnalytics, loadingCtrl, alertController, navCtrl, platform, appVersion) {
        this.afProvider = afProvider;
        this.toast = toast;
        this.router = router;
        this.afAuth = afAuth;
        this.firestoreProvider = firestoreProvider;
        this.firebaseAnalytics = firebaseAnalytics;
        this.loadingCtrl = loadingCtrl;
        this.alertController = alertController;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.appVersion = appVersion;
        this.Hydration = 80;
        this.perceived_hyd = 90;
        this.sg = 15;
        this.weight_kg = 100;
        this.disSG = "1.015";
        this.hydMsg = "Mildly dehydrated.";
        this.desMsg = "My performance won't be affected.";
        this.newTest = false;
        this.oldTest = false;
        this.preTest = true;
        this.sub = new Subscription();
        this.subAuth = new Subscription();
    }
    NewTestPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('New Test Page Will Enter');
        this.firebaseAnalytics.setCurrentScreen('NewTestPage');
        this.mobilePlatform = this.platform.platforms();
        this.appVersion.getVersionNumber()
            .then(function (res) { return _this.versionNumber = res; })
            .catch(function (error) { return console.error(error); });
        this.firebaseAnalytics.logEvent('new_test_screen_loads', { uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
            .then(function (res) { return console.log(res); })
            .catch(function (error) { return console.error(error); });
    };
    NewTestPage.prototype.ionViewWillLeave = function () {
        console.log("New Test Page Will Leave");
        if (!this.sub.closed) {
            console.log("Unsubscribing from weight results...");
            this.sub.unsubscribe();
        }
        if (!this.subAuth.closed) {
            console.log("Unsubscribing from auth details...");
            this.subAuth.unsubscribe();
        }
    };
    NewTestPage.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Initializing New Test Page");
        this.subAuth = this.afAuth.authState.subscribe(function (user) {
            console.log("Subscribing to auth details...");
            if (user && user.uid) {
                _this.uid = user.uid;
                if (!_this.subAuth.closed) {
                    console.log("Unsubscribing from auth details...");
                    _this.subAuth.unsubscribe();
                }
                else {
                    console.log("Already unsubscribed from auth details!");
                }
                _this.weightEntry = _this.afProvider.getCurrentWeight(_this.uid).valueChanges();
                _this.sub = _this.weightEntry.subscribe(function (res) {
                    console.log("Subscribing to weight results...");
                    _this.weight_kg = res.weight_kg;
                    if (!_this.sub.closed) {
                        console.log("Unsubscribing from weight results...");
                        _this.sub.unsubscribe();
                    }
                    if (!_this.subAuth.closed) {
                        console.log("Unsubscribing from auth details...");
                        _this.subAuth.unsubscribe();
                    }
                    else {
                        console.log("Already unsubscribed from auth details!");
                    }
                });
            }
            else {
                console.log("Could not find authentication details");
                _this.router.navigateByUrl("/login");
            }
        });
    };
    NewTestPage.prototype.ngOnDestroy = function () {
        console.log("Destroying New Test Page");
    };
    NewTestPage.prototype.updateMsg = function () {
        switch (this.perceived_hyd) {
            case 70:
                this.hydMsg = "Severely dehydrated.";
                this.desMsg = "I'm not feeling well.";
                break;
            case 80:
                this.hydMsg = "Dehydrated.";
                this.desMsg = "I'm not quite ready to perform.";
                break;
            case 90:
                this.hydMsg = "Mildly dehydrated.";
                this.desMsg = "My performance won't be affected.";
                break;
            case 100:
                this.hydMsg = "Fully hydrated!";
                this.desMsg = "I'm ready to perform.";
                break;
            case 110:
                this.hydMsg = "Over-hydrated.";
                this.desMsg = "I'm feeling bloated.";
                break;
            default:
                this.hydMsg = "Error: hydration unknown.";
                this.desMsg = "Something went wrong.";
        }
    };
    NewTestPage.prototype.updateSG = function () {
        this.disSG = (this.sg / 1000 + 1).toFixed(3);
    };
    NewTestPage.prototype.startTest = function () {
        this.preTest = false;
        this.newTest = true;
        this.firebaseAnalytics.logEvent('start_test_button_pressed', { uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
            .then(function (res) { return console.log(res); })
            .catch(function (error) { return console.error(error); });
        this.firebaseAnalytics.logEvent('either_test_button_pressed', { uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
            .then(function (res) { return console.log(res); })
            .catch(function (error) { return console.error(error); });
        this.presentAlertConfirm();
    };
    NewTestPage.prototype.addResults = function () {
        this.preTest = false;
        this.oldTest = true;
        this.firebaseAnalytics.logEvent('previous_results_button_pressed', { uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
            .then(function (res) { return console.log(res); })
            .catch(function (error) { return console.error(error); });
        this.firebaseAnalytics.logEvent('either_test_button_pressed', { uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
            .then(function (res) { return console.log(res); })
            .catch(function (error) { return console.error(error); });
    };
    //see how this works
    NewTestPage.prototype.presentAlertConfirm = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Hold Up!',
                            message: 'Wait 30s before checking results',
                            buttons: [
                                {
                                    text: 'I Already Did',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        console.log('Confirm Cancel: blah');
                                        _this.loadingModalDisplayed = false;
                                    }
                                }, {
                                    text: 'Start Timer',
                                    handler: function () {
                                        console.log('Confirm Okay');
                                        _this.loadingModalDisplayed = true;
                                        _this.presentLoading();
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //backdropDismiss: true,
    NewTestPage.prototype.presentLoading = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loader;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({
                            message: "Please wait 30s...",
                            duration: 30000
                        })];
                    case 1:
                        loader = _a.sent();
                        return [4 /*yield*/, loader.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    NewTestPage.prototype.reset = function () {
        this.preTest = true;
        this.oldTest = false;
        this.newTest = false;
    };
    NewTestPage.prototype.updateDB = function () {
        return __awaiter(this, void 0, void 0, function () {
            var diff, date, loading;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.Hydration = this.firestoreProvider.getHydration(this.sg);
                        diff = this.perceived_hyd - this.Hydration;
                        this.firebaseAnalytics.logEvent('add_results_button_pressed', { Is_Old_Test: this.oldTest, Perception_Difference: diff, SG_Reading: this.sg, uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
                            .then(function (res) { return console.log(res); })
                            .catch(function (error) { return console.error(error); });
                        if (this.oldTest) {
                            //return test time
                            //need to get utc offset
                            if (this.myDate.includes("undefined")) {
                                this.presentToast("Please enter the date of the test and try again");
                                return [2 /*return*/];
                            }
                            if (this.myTime.includes("undefined")) {
                                this.presentToast("Please enter the time of the test and try again");
                                return [2 /*return*/]; //stop test
                            }
                            this.testTime = this.myDate + "T" + this.myTime + 'Z';
                            this.firebaseAnalytics.logEvent('old_results_added', { Is_Old_Test: this.oldTest, Perception_Difference: diff, SG_Reading: this.sg, uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
                                .then(function (res) { return console.log(res); })
                                .catch(function (error) { return console.error(error); });
                        }
                        else {
                            date = new Date();
                            this.testTime = date.toISOString();
                            this.firebaseAnalytics.logEvent('new_results_added', { Loading_Modal_Displayed: this.loadingModalDisplayed, Is_Old_Test: this.oldTest, Perception_Difference: diff, SG_Reading: this.sg, uid: this.uid, platform: this.mobilePlatform, appVersion: this.versionNumber })
                                .then(function (res) { return console.log(res); })
                                .catch(function (error) { return console.error(error); });
                        }
                        console.log("Adding Results...");
                        return [4 /*yield*/, this.loadingCtrl.create()];
                    case 1:
                        loading = _a.sent();
                        this.firestoreProvider.addTestResults(this.uid, this.sg, this.testTime, this.weight_kg, this.perceived_hyd).then(function () {
                            loading.dismiss().then(function () {
                                _this.presentToast('Success!');
                                console.log("Results added!");
                                _this.router.navigateByUrl('/home');
                            });
                        }, function (error) {
                            console.error(error);
                        });
                        return [4 /*yield*/, loading.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    NewTestPage.prototype.presentToast = function (msg) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toast.create({
                            message: msg,
                            duration: 1500,
                            position: 'bottom'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    NewTestPage = __decorate([
        Component({
            selector: 'app-new-test',
            templateUrl: './new-test.page.html',
            styleUrls: ['./new-test.page.scss'],
        }),
        __metadata("design:paramtypes", [FirestoreService, ToastController, Router,
            AngularFireAuth, FirestoreService, FirebaseAnalytics,
            LoadingController, AlertController, NavController, Platform, AppVersion])
    ], NewTestPage);
    return NewTestPage;
}());
export { NewTestPage };
//# sourceMappingURL=new-test.page.js.map