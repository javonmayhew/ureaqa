var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { NavController, ToastController, MenuController } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics/ngx';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
var LoginPage = /** @class */ (function () {
    function LoginPage(afAuth, db, firebaseAnalytics, toastCtrl, router, navCtrl, menuCtrl) {
        this.afAuth = afAuth;
        this.db = db;
        this.firebaseAnalytics = firebaseAnalytics;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.user = {};
        this.sub = new Subscription();
        this.passwordReset = false;
    }
    LoginPage.prototype.ngOnInit = function () {
        console.log('Initializing Login Page...');
        this.firebaseAnalytics.setCurrentScreen('LoginPage');
        this.menuCtrl.enable(false);
    };
    LoginPage.prototype.ionViewWillLeave = function () {
        console.log("Login Page Will Leave");
        if (!this.sub.closed) {
            console.log("Unsubscribing from test results...");
            this.sub.unsubscribe();
        }
    };
    LoginPage.prototype.ngOnDestroy = function () {
        //unsubscribe!!
        console.log('Destroying Login Page...');
        if (this.sub) {
            this.sub.unsubscribe();
        }
    };
    LoginPage.prototype.login = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        user.email = user.email.trim();
                        user.password = user.password.trim();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)];
                    case 2:
                        result = _a.sent();
                        console.log(result);
                        if (result.user.uid) {
                            this.userId = result.user.uid;
                            this.menuCtrl.enable(true);
                            if (result.user.emailVerified) {
                                //check if first login
                                this.profile = this.db.getProfile(this.userId).valueChanges();
                                this.sub = this.profile.subscribe(function (res) {
                                    if (res && res.username.length > 4) {
                                        //profile exists
                                        console.log(res.username + " signed in");
                                        _this.router.navigateByUrl('/home');
                                    }
                                    else {
                                        //set up profile
                                        _this.router.navigateByUrl('/create-profile');
                                    }
                                });
                            }
                            else {
                                this.afAuth.auth.currentUser.sendEmailVerification().then(function () {
                                    _this.presentToast("Verify your email address first.");
                                }, function (error) {
                                    console.log(error);
                                    _this.presentToast("Error creating profile.");
                                });
                            }
                        }
                        else {
                            this.presentToast("Unknown login error");
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        console.error(e_1);
                        this.presentToast(e_1.message);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.register = function () {
        this.router.navigateByUrl('/register'); //push
    };
    LoginPage.prototype.resetPassword = function () {
        this.passwordReset = true;
    };
    LoginPage.prototype.sendPasswordEmail = function (email) {
        email = email.trim();
        this.afAuth.auth.sendPasswordResetEmail(email)
            .then(function (res) { return console.log(res); })
            .catch(function (error) { return console.error(error); });
        this.presentToast("Check your email to reset your password");
    };
    LoginPage.prototype.presentToast = function (msg) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: 3000,
                            position: 'middle'
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = __decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss'],
        }),
        __metadata("design:paramtypes", [AngularFireAuth, FirestoreService, FirebaseAnalytics,
            ToastController, Router,
            NavController, MenuController])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.page.js.map