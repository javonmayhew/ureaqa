var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import * as moment from 'moment';
import { NotifyService } from '../services/notify.service';
var NotificationSetupModalPage = /** @class */ (function () {
    function NotificationSetupModalPage(modalCtrl, notify, toast) {
        this.modalCtrl = modalCtrl;
        this.notify = notify;
        this.toast = toast;
        this.notifications = [];
        this.notifyTime = moment(new Date()).format();
        this.chosenHours = new Date().getHours();
        this.chosenMinutes = new Date().getMinutes();
        this.days = [
            { title: 'Monday', dayCode: 1, checked: false },
            { title: 'Tuesday', dayCode: 2, checked: false },
            { title: 'Wednesday', dayCode: 3, checked: false },
            { title: 'Thursday', dayCode: 4, checked: false },
            { title: 'Friday', dayCode: 5, checked: false },
            { title: 'Saturday', dayCode: 6, checked: false },
            { title: 'Sunday', dayCode: 0, checked: false }
        ];
    }
    NotificationSetupModalPage.prototype.ngOnInit = function () {
    };
    NotificationSetupModalPage.prototype.closeModal = function () {
        this.modalCtrl.dismiss();
    };
    NotificationSetupModalPage.prototype.timeChange = function () {
        this.time = this.notifyTime;
        console.log(this.time);
        this.time = new Date(this.notifyTime);
        console.log(this.time);
    };
    NotificationSetupModalPage.prototype.cancelAll = function () {
        console.log("Cancel all");
        this.notify.cancelAll();
    };
    NotificationSetupModalPage.prototype.testNotification1 = function () {
        this.notify.testNotification(5000); //5 seconds
    };
    NotificationSetupModalPage.prototype.testNotification2 = function () {
        this.notify.testNotification(300000); // 5 minutes
    };
    NotificationSetupModalPage.prototype.testCheckNotifications = function () {
        this.notify.checkNotifications(); //display alert with scheduled notifications
        this.presentToast();
    };
    NotificationSetupModalPage.prototype.presentToast = function () {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toast.create({
                            message: 'Notifications Checked',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    NotificationSetupModalPage.prototype.addNotifications = function () {
        console.log("Add things");
        console.log(this.days);
        console.log(this.time);
        this.notify.addNotifications(this.days, this.time);
        this.closeModal();
    };
    NotificationSetupModalPage = __decorate([
        Component({
            selector: 'app-notification-setup-modal',
            templateUrl: './notification-setup-modal.page.html',
            styleUrls: ['./notification-setup-modal.page.scss'],
        }),
        __metadata("design:paramtypes", [ModalController, NotifyService, ToastController])
    ], NotificationSetupModalPage);
    return NotificationSetupModalPage;
}());
export { NotificationSetupModalPage };
//# sourceMappingURL=notification-setup-modal.page.js.map