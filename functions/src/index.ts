import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();
const db = admin.firestore();
import * as Stripe from 'stripe';
//import { StreamPriorityOptions } from 'http2';
//import { DocumentReference } from '@google-cloud/firestore';
//import { response } from 'express';

const stripe = new Stripe(functions.config().stripe.secret);

//1. create customer
export const createStripeCustomer = functions.auth
  .user()
  .onCreate(async (userRecord, context) => {
    const firebaseUID = userRecord.uid;
    //check if brady made the customer already
    const check = await stripe.customers.list({email: userRecord.email});
    let customer: Stripe.customers.ICustomer;

    console.log(check);
    //if he didn't
    if (check.data.length<1){
      //create a new customer
      console.log("Creatiing a new customer");
      customer = await stripe.customers.create({
        email: userRecord.email,
        metadata: { firebaseUID: firebaseUID }

      });
      await db.doc(`users/${firebaseUID}`).set({
        stripeId: customer.id
      });
    }
    else{
      //get existing customer info
      console.log("Fetching existing customer info");
      customer = check.data[0];
      let sub;
      let status;
      let startDate;
      let periodEnd;
      let subscriptionId;
      let itemId;
      if(customer.subscriptions.data.length>0){
        sub=customer.subscriptions.data[0];
        console.log(sub);

        status= sub.status;
        startDate= sub.created;
        periodEnd= sub.current_period_end;
        subscriptionId= sub.id;
        itemId= sub.items.data[0].id;

      }
      else{
        status = "trialling";
        startDate = 1554076800; //Apr 1
        periodEnd = 1556668800; //May 1
        subscriptionId = "sub_0";
        itemId = "prod_0";
      }
      
      await db.doc(`users/${firebaseUID}`).set({
        stripeId: customer.id,
        status: status,
        startDate: startDate,
        periodEnd: periodEnd,
        subscriptionId: subscriptionId,
        itemId: itemId
      });     
    }  
    console.log("Creatiing customer in database");
    const docRef = db.doc(`customers/${customer.id}`);
    docRef.get().then((docSnapshot)=>{
      if (docSnapshot.exists){
        return db.doc(`customers/${customer.id}`).update({
          cusId: customer.id,
          uid: firebaseUID,
          email: userRecord.email
        });

      }
      else{
        return db.doc(`customers/${customer.id}`).create({
          cusId: customer.id,
          uid: firebaseUID,
          email: userRecord.email
        });

      }
    })
    .catch(err => console.error(err));
    
  });


//2. start subscription
export const startSubscription = functions.https.onCall(
  async (data, context) => {
    console.log(data.user);
    const userId = data.user.id; 
    const user = data.user;
    console.log(user);
    console.log("That's the user data");
    
    const profile = await stripe.customers.update(user.stripeId,{
      shipping:{address:{line1: user.address, city:user.city, country:user.country, postal_code: user.postalCode, state: user.province }, 
      name: user.name, phone: user.phoneNumber}, metadata: {firebaseUID: userId},
    });

      
    console.log(profile);
    console.log(profile.subscriptions.data)
    let planCheck;
    if (profile.subscriptions.data.length >0){
      planCheck = profile.subscriptions.data[0].items.data[0].plan;
      console.log(planCheck);
      console.log("user already has a subscription");
      const sub1 = profile.subscriptions.data[0];
      console.log(sub1);

      return db.doc(`users/${userId}`).update({
        status: sub1.status,
        startDate: sub1.created,
        periodEnd: sub1.current_period_end,
        subscriptionId: sub1.id,
        itemId: sub1.items.data[0].plan.id
      
      });
      

    }
    else{
      planCheck = "defaultPlan";
    }
    
    // Attach the card to the user
    const source = await stripe.customers.createSource(user.stripeId, {
      source: data.source
    });
 
    if (!source) {
      throw new Error('Stripe failed to attach card');
    }
  
    // Subscribe the user to the plan
    if (planCheck !== user.plan){

      const sub = await stripe.subscriptions.create({
        customer: user.stripeId,
        items: [{ plan: data.user.plan }]
      });

      console.log(sub);
    
      // Update user document
      return db.doc(`users/${userId}`).update({
        status: sub.status,
        startDate: sub.created,
        periodEnd: sub.current_period_end,
        subscriptionId: sub.id,
        itemId: sub.items.data[0].plan.id
      }); 
    }
    else{
      return;
    }        
  }
);


//3. update subscription
export const recurringPayment = functions.https.onRequest(async (req, resp) => {
  
  const hook = req.body.type;
  const data = req.body.data.object;
  if (!data) throw new Error('Missing data');
  console.log(data);
  console.log(hook);
  let status = data.status;
  let expiry = data.period_end;
  //console.log(ref);
  let ref;
  console.log(data.customer + "- Expiry: "+expiry);
  let userId;
  const sub = await stripe.subscriptions.retrieve(data.subscription);
  console.log(sub);
  expiry = sub.current_period_end;
  console.log("Expiry: "+expiry);
  db.doc(`customers/${data.customer}`).get().then(async snapshot => {
    console.log(snapshot);
    const res = snapshot.data();
    if (res && res.uid){
      console.log("Customer data: "+res);
      userId = res.uid;
      ref = db.doc(`users/${userId}`);
    }
    else{
      console.log("Res is undefined: "+res);
      const cusInfo = await stripe.customers.retrieve(data.customer);
      userId = cusInfo.metadata.firebaseUID;
      ref = db.doc(`users/${userId}`);
    }
    // Payment succeeded webhook
    if(hook === 'invoice.payment_succeeded'){
      status = "Active";
    
        return ref.update({
          status: status,
          periodEnd: expiry
        });  
    }
    // Payment failed webhook
    if(hook === 'invoice.payment_failed'){
      status = "Past Due"
      return ref.update({
        status: status,
      });  
    }
    //something else
    throw new Error('Not a valid webhook');

  })
  .then(() =>resp.status(200).send(`Successfully handled ${hook}`))
  .catch(err=>resp.status(400).send(err));
  
});



//4. create customer from existing profile
export const createExistingStripeCustomer = functions.https.onCall(
  async (data, context) => {
    const email = data.email;
    const firebaseUID = data.uid;
    console.log(data);
    console.log(email);
    //check if brady made the customer already
    const check = await stripe.customers.list({email: email});
    let customer
    console.log(check);
    //if he didn't
    if (check.data.length<1){
      //create a new customer
      console.log("No customer exists! Let's create a new one!");
      customer = await stripe.customers.create({
        email: email,
        metadata: { firebaseUID }

      });
      console.log(customer);
      await db.doc(`users/${firebaseUID}`).update({
        stripeId: customer.id,
        status: "trialling",
      });
      console.log("Updated firebase");
      return db.doc(`customers/${customer.id}`).set({
        cusId: customer.id,
        uid: firebaseUID,
        email: email
      });
    }
    else{
       //get existing customer info
       console.log("Fetching existing customer info");
       customer = check.data[0];
       let sub;
       let status;
       let startDate;
       let periodEnd;
       let subscriptionId;
       let itemId;
       if(customer.subscriptions.data.length>0){
         sub=customer.subscriptions.data[0];
         console.log(sub);
 
         status= sub.status;
         startDate= sub.created;
         periodEnd= sub.current_period_end;
         subscriptionId= sub.id;
         itemId= sub.items.data[0].id;
 
       }
       else{
         status = "trialling";
         startDate = 1554076800; //Apr 1
         periodEnd = 1556668800; //May 1
         subscriptionId = "sub_0";
         itemId = "prod_0";
       }
       
       await db.doc(`users/${firebaseUID}`).update({
         stripeId: customer.id,
         status: status,
         startDate: startDate,
         periodEnd: periodEnd,
         subscriptionId: subscriptionId,
         itemId: itemId
       });     
       
     console.log("Creatiing customer in database");
     return db.doc(`customers/${customer.id}`).set({
       cusId: customer.id,
       uid: firebaseUID,
       email: email
     });
    }   
  });

